package org.callistasoftware.android.c2dm;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

/**
 * Receives push notifications and registration events from a
 * Google C2DM server
 * 
 * @author Marcus Krantz [marcus.krantz@callistaenterprise.se]
 *
 */
public abstract class C2DMReceiver extends BroadcastReceiver  {

	private static final String TAG = C2DMReceiver.class.getSimpleName();

	@Override
	public void onReceive(Context context, Intent intent) {
		
		Log.d(TAG, "Received broadcast event. Start push service");
		
		/*
		 * Find our push service
		 */
		intent.setClass(context, getPushServiceClass());
		context.startService(intent);
        
		/*
		 * Set success
		 */
		setResult(Activity.RESULT_OK, null, null);   
	}
	
	/**
	 * Method that returns the implementation class of
	 * the push service. This is needed to send an explicit
	 * intent to the implemented push service
	 * @return
	 */
	public abstract Class<?> getPushServiceClass();
}
