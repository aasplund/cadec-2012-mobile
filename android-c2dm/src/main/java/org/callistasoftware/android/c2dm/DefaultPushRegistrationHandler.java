package org.callistasoftware.android.c2dm;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

/**
 * A default implementation of a push registration handler. The responsibility
 * of this class is to send registration and unregistration intents for c2dm 
 * to the platform.
 * 
 * @author Marcus Krantz [marcus.krantz@callistaenterprise.se] 
 *
 */
public class DefaultPushRegistrationHandler implements PushRegistrationHandler {

	private static final String TAG = DefaultPushRegistrationHandler.class.getSimpleName();
	
	private static final String C2DM_REG_INTENT = "com.google.android.c2dm.intent.REGISTER";
	private static final String C2DM_UNREG_INTENT = "com.google.android.c2dm.intent.UNREGISTER";
	
	@Override
	public void registerAppForPush(Context context, final String pushEmail) {
		Log.i(TAG, "Registering app for push notifications");
		final Intent regIntent = new Intent(C2DM_REG_INTENT);
		regIntent.putExtra("app", PendingIntent.getBroadcast(context, 0, new Intent(), 0));
		regIntent.putExtra("sender", pushEmail);
		
		context.startService(regIntent);
	}

	@Override
	public void unregisterAppForPush(final Context context) {
		Log.i(TAG, "Unregistering app for push notifications");
		Intent unregIntent = new Intent(C2DM_UNREG_INTENT);
		unregIntent.putExtra("app", PendingIntent.getBroadcast(context, 0, new Intent(), 0));
		
		context.startService(unregIntent);
	}
}
