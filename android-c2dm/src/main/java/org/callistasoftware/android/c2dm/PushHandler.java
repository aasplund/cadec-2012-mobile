package org.callistasoftware.android.c2dm;

import android.content.Context;
import android.content.Intent;

/**
 * Interface that defines a set of methods to handle all kinds
 * of event processing for Googles C2DM.
 * 
 * @author Marcus Krantz [marcus.krantz@callistaenterprise.se]
 */
public interface PushHandler {

	/**
	 * Executed when a c2dm server has delivered a c2dm message 
	 * (push notification)
	 * @param context
	 * @param intent
	 */
	void handlePushEvent(final Context context, final Intent intent);
	
	/**
	 * Executed when a c2dm server has delivered a registration id to the
	 * app. The registration id is
	 * @param context
	 * @param intent
	 */
	void handlePushRegistration(final Context context, final Intent intent, final String registrationId, final String server);
	
	/**
	 * Executed when a c2dm server has delivered an unregistration event 
	 * to the app.
	 * @param context
	 * @param intent
	 */
	void handlePushUnregistration(final Context context, final Intent intent);
}
