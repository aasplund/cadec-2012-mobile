package org.callistasoftware.android.c2dm;

import android.content.Context;

/**
 * Interface that exposes methods to register
 * and unregister the app for push notifications
 * 
 * @author Marcus Krantz [marcus.krantz@callistaenterprise.se]
 *
 */
public interface PushRegistrationHandler {

	/**
	 * Registers the device for push notifications
	 */
	void registerAppForPush(final Context context, final String pushEmail);
	
	/**
	 * Unregisters the device for push notifications
	 */
	void unregisterAppForPush(final Context context);
}
