package org.callistasoftware.android.c2dm;

import android.app.IntentService;
import android.content.Intent;
import android.util.Log;

/**
 * Push Service
 * This class must be subclassed. The subclass will register app
 * dependent implementations of a push handler and a push registration
 * handler. It must also provide a a valid google account that has
 * accessed to c2dm as well as a address of a server that will be passed
 * the registration id.
 * 
 * @author Marcus Krantz [marcus.krantz@callistaenterprise.se]
 *
 */
public abstract class PushService extends IntentService {
	
	private static final String TAG = PushService.class.getSimpleName();
	
	public static final String REGISTER_APP_FOR_PUSH = "org.callistasoftware.android.intent.REGISTER_APP_FOR_PUSH";
	public static final String UNREGISTER_APP_FOR_PUSH = "org.callistasoftware.android.intent.UNREGISTER_APP_FOR_PUSH";
	
	private static final String C2DM_REGISTRATION_INTENT = "com.google.android.c2dm.intent.REGISTRATION";
	private static final String C2DM_UNREGISTRATION_INTENT = "com.google.android.c2dm.intent.UNREGISTER";
	private static final String C2DM_MESSAGE = "com.google.android.c2dm.intent.RECEIVE";
	
	private PushRegistrationHandler registrationHandler;
	private PushHandler handler;
	
	private String pushRegistrationUrl;
	private String pushEmail;
	
	public PushService() {
		super("PushService");
	}
	
	@Override
	protected void onHandleIntent(Intent intent) {
		
		if (intent.getAction() == null) {
			Log.w(TAG, "Received intent without an action. Nothing to do.");
			return;
		}
		
		Log.i(TAG, "Processing intent. Action: " + intent.getAction());
		
		if (intent.getAction().equals(REGISTER_APP_FOR_PUSH)) {
			this.getPushRegistrationHandler().registerAppForPush(this, this.getPushEmail());
			return;
		}
		
		if (intent.getAction().equals(UNREGISTER_APP_FOR_PUSH)) {
			this.getPushRegistrationHandler().unregisterAppForPush(this);
			return;
		}
		
		if (intent.getAction().equals(C2DM_REGISTRATION_INTENT)) {
			Log.d(TAG, "Push registration intent delegated to callback for further processing");
			
			/*
			 * Check for error
			 */
			if (intent.getStringExtra("error") != null) {
				Log.w(TAG, "Registration failed. Error: " + intent.getStringExtra("error"));
				return;
			}
			
			String registration = intent.getStringExtra("registration_id");
			Log.d(TAG, "Registration id is: " + registration);
			this.getPushHandler().handlePushRegistration(this, intent, registration, this.getPushRegistrationUrl());
			return;
		}
		
		if (intent.getAction().equals(C2DM_UNREGISTRATION_INTENT)) {
			Log.d(TAG, "Push unregistration intent delegated to callback for further processing");
			this.getPushHandler().handlePushUnregistration(this, intent);
			return;
		}
		
		if (intent.getAction().equals(C2DM_MESSAGE)) {
			Log.d(TAG, "Push notification intent delegated to callback for further processing");
			this.getPushHandler().handlePushEvent(this, intent);
			return;
		}	
	}
	
	public PushRegistrationHandler getPushRegistrationHandler() {
		if (this.registrationHandler == null) {
			this.registrationHandler = this.registerPushRegistrationHandler();
		}
		
		return this.registrationHandler;
	}
	
	public PushHandler getPushHandler() {
		if (this.handler == null) {
			this.handler = this.registerPushHandler();
		}
		
		return this.handler;
	}
	
	public String getPushRegistrationUrl() {
		if (this.pushRegistrationUrl == null) {
			this.pushRegistrationUrl = this.registerPushRegistrationUrl();
		}
		
		return this.pushRegistrationUrl;
	}
	
	public String getPushEmail() {
		if (this.pushEmail == null) {
			this.pushEmail = this.registerPushEmail();
		}
		
		return this.pushEmail;
	}
	
	/**
	 * Register a push registration handler implementation that will
	 * be used.
	 * @return
	 */
	public abstract PushRegistrationHandler registerPushRegistrationHandler();
	
	/**
	 * Register a push handler implementation that will act upon push
	 * notifications and registration events.
	 * @return
	 */
	public abstract PushHandler registerPushHandler();
	
	/**
	 * Register the email address that will be used when
	 * requesting a c2dm device registration id
	 * @return
	 */
	public abstract String registerPushEmail();
	
	/**
	 * Register a server url that can be used to register
	 * the device's given registration id
	 * @return
	 */
	public abstract String registerPushRegistrationUrl();
}
