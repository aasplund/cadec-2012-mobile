package org.callistasoftware.netcare.android;

import org.callistasoftware.android.c2dm.PushService;
import org.callistasoftware.netcare.android.dto.Patient;
import org.callistasoftware.netcare.android.net.HttpClientConfiguration;
import org.callistasoftware.netcare.android.net.HttpConfigurationFactory;
import org.callistasoftware.netcare.android.push.NetcarePushService;
import org.callistasoftware.netcare.android.serviceclient.ServiceFactory;
import org.callistasoftware.netcare.android.serviceclient.ServiceInterface;
import org.callistasoftware.netcare.android.serviceclient.ServiceResult;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

/**
 * 
 * Handle the login for the netcare application.
 * 
 */
public class LoginActivity extends Activity {

	private static final String TAG = LoginActivity.class.getSimpleName();
	
	private EditText ffUsername;
	private EditText ffPassword;
	private Button btnLogin;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		final Intent i = new Intent(this.getApplicationContext(), NetcarePushService.class);
		i.setAction(PushService.REGISTER_APP_FOR_PUSH);
		startService(i);
		
		setContentView(R.layout.main);
		ffUsername = (EditText) findViewById(R.id.username);
		ffPassword = (EditText) findViewById(R.id.password);
		btnLogin = (Button) findViewById(R.id.login_button);
		
		btnLogin.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				
				final String username = ffUsername.getText().toString();
				final String password = ffPassword.getText().toString();
	
				new ServiceCallTask<Patient>(LoginActivity.this, new ServiceCallback<Patient>() {
					
					@Override
					public void onSuccess(ServiceResult<Patient> result) {
						// Save credentials
						Log.d(TAG, "Storing user's credentials...");
						final Editor editor = LoginActivity.this.getApplicationContext().getSharedPreferences("NETCARE", 0).edit();
						editor.putLong("userId", result.getData().getId());
						editor.putString("username", result.getData().getCivicRegistrationNumber());
						editor.putString("pincode", result.getData().getSecurityCode());
						editor.commit();
						
						// Successful login. Start new activity
						startActivity(new Intent(getApplicationContext(), OrdinationListActivity.class));
					}
					
					@Override
					public ServiceResult<Patient> doCall() {
						// Configuration for our HttpClient
						final HttpClientConfiguration config = HttpConfigurationFactory.newSecureConfigurationWithBasicAuthentication(9443
								, username
								, password
								, R.raw.clienttruststore
								, "password");
						
						// Get the service interface. Pass in our http client configuration
						final ServiceInterface login = ServiceFactory.createNewServiceInterface(getApplicationContext(), config);
						
						// Login
						return login.login(username, password);
					}

					@Override
					public String getProgressMessage() {
						return getApplicationContext().getString(R.string.login_progress);
					}
				}).execute();
			}
		});
	}
}
