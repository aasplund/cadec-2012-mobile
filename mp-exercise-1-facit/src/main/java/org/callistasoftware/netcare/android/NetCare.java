package org.callistasoftware.netcare.android;

import android.app.Application;
import android.content.res.Configuration;

/**
 * The main application class for the Netcare application.
 * 
 */
public class NetCare extends Application {

	private static NetCare singleton;

	/**
	 * An application object must always be a static singleton.
	 */
	public static NetCare getInstance() {
		return singleton;
	}

	/**
	 * Called when the application is created. Override this method to
	 * initialize your application singleton and create and initialize any
	 * application state variables or shared resources.
	 */
	@Override
	public void onCreate() {
		super.onCreate();
		singleton = this;
	}

	/**
	 * Can be called when the application object is terminated. Note that there
	 * is no guarantee of this method handlers being called. It the application
	 * is terminated by the kernel in order to free resources for other
	 * applications, the process will be terminated without warning and without
	 * a call to the application objects onTerminate handler.
	 */
	@Override
	public void onTerminate() {
		super.onTerminate();
	}

	/**
	 * Provides an opportunity for well-behaved applications to free additional
	 * memory when the system is running low on resources. This will generally
	 * only be called when background processes have already been terminated and
	 * the current foreground applications are still low on memory. Override
	 * this handler to clear caches or release unnecessary resources.
	 */
	@Override
	public void onLowMemory() {
		super.onLowMemory();
	}

	/**
	 * Unlike with activities, your application object is not killed and
	 * restarted for configuration changes. Override this handler if it is
	 * necessary to handle configuration changes at an application level.
	 */
	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
	}

}
