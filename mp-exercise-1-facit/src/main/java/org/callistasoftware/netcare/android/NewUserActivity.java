package org.callistasoftware.netcare.android;

import org.callistasoftware.netcare.android.dto.Patient;
import org.callistasoftware.netcare.android.net.HttpClientConfiguration;
import org.callistasoftware.netcare.android.net.HttpConfigurationFactory;
import org.callistasoftware.netcare.android.serviceclient.ServiceFactory;
import org.callistasoftware.netcare.android.serviceclient.ServiceInterface;
import org.callistasoftware.netcare.android.serviceclient.ServiceResult;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class NewUserActivity extends Activity {

	private EditText name;
	private EditText username;
	private EditText password;
	
	private Button createUser;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.newuser);
		
		this.name = (EditText) this.findViewById(R.id.createFullname);
		this.username = (EditText) this.findViewById(R.id.createUsername);
		this.password = (EditText) this.findViewById(R.id.createPassword);
		
		this.createUser = (Button) this.findViewById(R.id.create_button);
		this.createUser.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				final String name = NewUserActivity.this.name.getText().toString();
				final String username = NewUserActivity.this.username.getText().toString();
				final String password = NewUserActivity.this.password.getText().toString();
				
				if (name == null || username == null || password == null) {
					Toast.makeText(getApplicationContext(), "All fields are required. Please enter account information", 5000);
				}
				
				new ServiceCallTask<Patient>(NewUserActivity.this, new ServiceCallback<Patient>() {
					
					@Override
					public void onSuccess(ServiceResult<Patient> result) {
						startActivity(new Intent(NewUserActivity.this, LoginActivity.class));
					}
					
					@Override
					public ServiceResult<Patient> doCall() {
						final HttpClientConfiguration config = HttpConfigurationFactory.newSecureConfiguration(9443, R.raw.clienttruststore, "password");
						final ServiceInterface service = ServiceFactory.createNewServiceInterface(getApplicationContext(), config);
						
						final Patient p = new Patient();
						p.setName(name);
						p.setCivicRegistrationNumber(username);
						p.setSecurityCode(password);
						
						final ServiceResult<Patient> result = service.createNewUser(p);
						if (result.isSuccess()) {
							
							/*
							 * Store the user's id
							 */
							final Editor editor = NewUserActivity.this.getApplicationContext().getSharedPreferences("NETCARE", 0).edit();
							editor.putLong("userid", result.getData().getId());
							editor.commit();
						}
						
						return result;
					}

					@Override
					public String getProgressMessage() {
						return getApplicationContext().getString(R.string.create_user_progress);
					}
				}).execute();
			}
		});
	}
}
