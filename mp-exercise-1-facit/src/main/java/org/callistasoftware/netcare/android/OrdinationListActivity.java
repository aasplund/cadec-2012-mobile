package org.callistasoftware.netcare.android;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.callistasoftware.netcare.android.adapter.OrdinationListAdapter;
import org.callistasoftware.netcare.android.dto.Ordination;
import org.callistasoftware.netcare.android.net.HttpClientConfiguration;
import org.callistasoftware.netcare.android.net.HttpConfigurationFactory;
import org.callistasoftware.netcare.android.serviceclient.ServiceFactory;
import org.callistasoftware.netcare.android.serviceclient.ServiceInterface;
import org.callistasoftware.netcare.android.serviceclient.ServiceResult;

import android.app.ListActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ListAdapter;

public class OrdinationListActivity extends ListActivity {

	private static final String TAG = OrdinationListActivity.class.getSimpleName();
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		Log.i(TAG, "Displaying ordination list");
		setContentView(R.layout.ordination_list);
		
		new ServiceCallTask<Ordination[]>(OrdinationListActivity.this, new ServiceCallback<Ordination[]>() {

			@Override
			public ServiceResult<Ordination[]> doCall() {
				final Map<String, Object> params = new HashMap<String, Object>();
				params.put("patientId", getSharedPreferences("NETCARE", 0).getLong("userid", -1));	
				
				final String username = getSharedPreferences("NETCARE", 0).getString("username", "unknown");
				final String pincode = getSharedPreferences("NETCARE", 0).getString("pincode", "unknown");
				
				final HttpClientConfiguration config = HttpConfigurationFactory.newSecureConfigurationWithBasicAuthentication(9443, username, pincode, R.raw.clienttruststore, "password");
				
				final Long patientId = (Long) params.get("patientId");
				final ServiceInterface service = ServiceFactory.createNewServiceInterface(getApplicationContext(), config);
				Log.d(TAG, "New service interface called");
				
				return service.fetchOrdinations(patientId);
			}

			@Override
			public void onSuccess(ServiceResult<Ordination[]> result) {
				Log.d(TAG, "Ordinations size: " + result.getData().length);
				
				final ListAdapter adapter = new OrdinationListAdapter(getApplicationContext(), Arrays.asList(result.getData()));
				setListAdapter(adapter);
			}

			@Override
			public String getProgressMessage() {
				return getApplicationContext().getString(R.string.load_ordination_progress);
			}
		}).execute();
	}
}
