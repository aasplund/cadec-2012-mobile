package org.callistasoftware.netcare.android;

import org.callistasoftware.netcare.android.serviceclient.ServiceResult;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;

public class ServiceCallTask<T> extends AsyncTask<Void, Void, ServiceResult<T>> {

	private Context ctx;
	private ServiceCallback<T> callback;
	private ProgressDialog p;
	
	public ServiceCallTask(final Context ctx, final ServiceCallback<T> callback) {
		this.ctx = ctx;
		this.callback = callback;
	}
	
	@Override
	protected void onPreExecute() {
		p = ProgressDialog.show(this.ctx, "", this.callback.getProgressMessage(), true, false);
	}
	
	@Override
	protected ServiceResult<T> doInBackground(Void... params) {
		return this.callback.doCall();
	}
	
	@Override
	protected void onPostExecute(ServiceResult<T> result) {
		
		p.dismiss();
		
		if (result.isSuccess()) {
			
			this.callback.onSuccess(result);
			
		} else {
			
			final AlertDialog.Builder builder = new AlertDialog.Builder(this.ctx);
			builder.setMessage(result.getErrorMessage())
			.setTitle("Anropet misslyckades")
			.setPositiveButton("OK", new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					dialog.cancel();
				}
			});
			
			builder.create().show();
		}
	}

}
