package org.callistasoftware.netcare.android;

import org.callistasoftware.netcare.android.serviceclient.ServiceResult;

public interface ServiceCallback<T> {

	String getProgressMessage();
	
	ServiceResult<T> doCall();
	
	void onSuccess(ServiceResult<T> result);
}
