package org.callistasoftware.netcare.android.adapter;

import java.util.List;

import org.callistasoftware.netcare.android.R;
import org.callistasoftware.netcare.android.dto.Ordination;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

public class OrdinationListAdapter extends BaseAdapter {

	private static final String TAG = OrdinationListAdapter.class.getSimpleName();
	
	private Context mContext;
	private List<Ordination> ordinations;
	
	public OrdinationListAdapter(Context context, final List<Ordination> ordinations) {
		this.mContext = context;
		this.ordinations = ordinations;
	}

	@Override
	public int getCount() {
		return this.ordinations.size();
	}

	@Override
	public Object getItem(int position) {
		return this.ordinations.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		final Ordination bv = this.ordinations.get(position);
		
		Log.d(TAG, "Get view for ordination " + bv.getTitle());
		
		if (convertView == null) {
			Log.d(TAG, "Convert view is null. Inflate from xml...");
			convertView = (LinearLayout) LayoutInflater.from(mContext).inflate(R.layout.ordination_list_item, parent, false);
		}
		
		final TextView title = (TextView) convertView.findViewById(R.id.title);
		final TextView description = (TextView) convertView.findViewById(R.id.description);
		
		title.setText(bv.getTitle());
		description.setText(bv.getDescription());
		
		Log.d(TAG, "Returning item (" + bv.getTitle() + ", " + bv.getDescription() + ")");
		
		return convertView;
	}
	
	

}
