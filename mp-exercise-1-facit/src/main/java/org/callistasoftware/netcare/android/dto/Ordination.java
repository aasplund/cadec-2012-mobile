package org.callistasoftware.netcare.android.dto;

import java.util.Date;

public class Ordination {

	private Long id;
	private String title;
	private String description;
	private Date due;
	private Date reminder;
	private String reported;
	
	public Long getId() {
		return this.id;
	}
	
	void setId(final Long id) {
		this.id = id;
	}

	public String getTitle() {
		return this.title;
	}
	
	void setTitle(final String title) {
		this.title = title;
	}

	public String getDescription() {
		return this.description;
	}
	
	void setDescription(final String description) {
		this.description = description;
	}

	public Date getDue() {
		return this.due;
	}
	
	void setDue(final Date d) {
		this.due = d;
	}

	public String getReported() {
		return this.reported;
	}
	
	void setReported(final String reported) {
		this.reported = reported;
	}
	
	public Date getReminder() {
		return this.reminder;
	}
	
	void setReminder(final Date reminder) {
		this.reminder = reminder;
	}

}
