package org.callistasoftware.netcare.android.net;

import java.io.IOException;
import java.io.InputStream;
import java.security.KeyStore;

import javax.net.ssl.HttpsURLConnection;

import org.apache.http.HttpException;
import org.apache.http.HttpRequest;
import org.apache.http.HttpRequestInterceptor;
import org.apache.http.auth.AuthState;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.protocol.ClientContext;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.conn.ssl.X509HostnameVerifier;
import org.apache.http.impl.auth.BasicScheme;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.SingleClientConnManager;
import org.apache.http.protocol.HttpContext;

import android.content.Context;
import android.util.Log;

/**
 * A custom http client implementation that uses the sepcified
 * {@link HttpClientConfiguration}
 * 
 * @author Marcus Krantz [marcus.krantz@callistaenterprise.se]
 *
 */
public class CustomHttpClient extends DefaultHttpClient {

	private static final String TAG = CustomHttpClient.class.getSimpleName();
	
	private Context mContext;
	private HttpClientConfiguration config;
	
	public void configure(final HttpClientConfiguration config, final Context context) {
		this.config = config;
		this.mContext = context;
		
		Log.d(TAG, "Checking whether to configure basic authentication: " + this.config.isBasicAuthentication());
		if (this.config.isBasicAuthentication()) {
			this.configureBasicAuthentication();
		}
	}
	
	public HttpClientConfiguration getConfiguration() {
		return this.config;
	}
	
	@Override
	protected ClientConnectionManager createClientConnectionManager() {
		Log.d(TAG, "Creating client connection manager");
		
		final SchemeRegistry registry = new SchemeRegistry();
		
		final int port = this.config.getUnsecurePort();
		final int securePort = this.config.getSecurePort();
		
		Log.d(TAG, "Port: " + port);
		Log.d(TAG, "Secure port: " + securePort);
		
		if (port > 0) {
			Log.d(TAG, "Adding http scheme for port " + port);
			registry.register(new Scheme("http", new PlainSocketFactory(), port));
		}
		
		if (this.config.isSecure()) {
			Log.d(TAG, "Adding https scheme for port " + securePort);
			registry.register(new Scheme("https", this.createSSLSocketFactory(), securePort));
		}
		
		return new SingleClientConnManager(getParams(), registry);
	}
	
	private SSLSocketFactory createSSLSocketFactory() {
		
		Log.d(TAG, "Creating SSL socket factory");
		
		KeyStore truststore = null;
		KeyStore keystore = null;
		if (this.config.isSecure()) {
			truststore = this.loadStore(this.config.getTruststoreResourceId(), this.config.getTruststorePassword(), "BKS");
		}
		
		if (this.config.isMutualAuthentication()) {
			keystore = this.loadStore(this.config.getKeystoreResourceId(), this.config.getKeystorePassword(), "BKS");
		}
		
		return this.createFactory(keystore, this.config.getKeystorePassword(), truststore);
	}
	
	private SSLSocketFactory createFactory(final KeyStore keystore, final String keystorePassword, final KeyStore truststore) {
		
		SSLSocketFactory factory;
		try {
			if (!this.config.isMutualAuthentication()) {
				factory = new SSLSocketFactory(truststore);
			} else {
				factory = new SSLSocketFactory(keystore, this.config.getKeystorePassword(), truststore);
			}
			
			factory.setHostnameVerifier((X509HostnameVerifier) SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
			HttpsURLConnection.setDefaultHostnameVerifier((X509HostnameVerifier) SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
		} catch (Exception e) {
			Log.e(TAG, "Caught exception when trying to create ssl socket factory. Reason: " + e.getMessage());
			throw new RuntimeException(e);
		}
		
		return factory;
	}
	
	private KeyStore loadStore(final int store, final String storePass, final String storeType) {
		KeyStore keystore;
		try {
			keystore = KeyStore.getInstance(storeType);
			final InputStream is = this.mContext.getResources().openRawResource(store);
			
			try {
				keystore.load(is, storePass.toCharArray());
				Log.d(TAG, "Key store loaded");
			} finally {
				is.close();
			}
			
		} catch (final Exception e) {
			Log.e(TAG, "Exception caught when loading key store. Reason: " + e.getMessage());
			throw new RuntimeException(e);
		}
		
		return keystore;
	}
	
	private void configureBasicAuthentication() {
		Log.d(TAG, "Configuring basic authentication...");
		this.addRequestInterceptor(new HttpRequestInterceptor() {
			
			@Override
			public void process(HttpRequest request, HttpContext context)
					throws HttpException, IOException {
				Log.d(TAG, "Request interceptor processing...");
				Log.d(TAG, "Username: " + CustomHttpClient.this.getConfiguration().getUsername() + " Password: " + CustomHttpClient.this.getConfiguration().getPassword());
				
				final AuthState state = (AuthState) context.getAttribute(ClientContext.TARGET_AUTH_STATE);
				state.setAuthScheme(new BasicScheme());
				state.setCredentials(new UsernamePasswordCredentials(CustomHttpClient.this.config.getUsername(), CustomHttpClient.this.config.getPassword()));
			}
		}, 0);
	}
}
