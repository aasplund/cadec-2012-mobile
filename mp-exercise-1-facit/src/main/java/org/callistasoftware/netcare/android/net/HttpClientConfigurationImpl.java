package org.callistasoftware.netcare.android.net;

class HttpClientConfigurationImpl implements HttpClientConfiguration {

	private int port;
	private int securePort;
	private String username;
	private String password;
	
	private int truststoreResourceId;
	private String truststorePassword;
	
	private int keystoreResourceId;
	private String keystorePassword;
	private String keyPassword;
	
	public HttpClientConfigurationImpl() {

	}
	
	@Override
	public void setBasicAuthenticationCredentials(final String username, final String password) {
		this.username = username;
		this.password = password;
	}

	@Override
	public void setSecurePort(int port) {
		this.securePort = port;
	}

	@Override
	public void setUnsecurePort(int port) {
		this.port = port;
	}

	@Override
	public void setTruststore(int truststoreResourceId,
			String truststorePassword) {
		this.truststoreResourceId = truststoreResourceId;
		this.truststorePassword = truststorePassword;
	}

	@Override
	public void setKeystore(int keystoreResourceId, String keystorePassword,
			String keyPassword) {
		this.keystoreResourceId = keystoreResourceId;
		this.keystorePassword = keystorePassword;
		this.keyPassword = keyPassword;
	}

	@Override
	public String getUsername() {
		return this.username;
	}

	@Override
	public String getPassword() {
		return this.password;
	}

	@Override
	public int getSecurePort() {
		return this.securePort;
	}

	@Override
	public int getUnsecurePort() {
		return this.port;
	}

	@Override
	public int getTruststoreResourceId() {
		return this.truststoreResourceId;
	}

	@Override
	public String getTruststorePassword() {
		return this.truststorePassword;
	}

	@Override
	public int getKeystoreResourceId() {
		return this.keystoreResourceId;
	}

	@Override
	public String getKeystorePassword() {
		return this.keystorePassword;
	}

	@Override
	public String getKeyPassword() {
		return this.keyPassword;
	}

	@Override
	public boolean isBasicAuthentication() {
		return (this.getUsername() != null) 
				&& (this.getPassword() != null);
	}

	@Override
	public boolean isSecure() {
		return this.securePort > 0;
	}

	@Override
	public boolean isMutualAuthentication() {
		return (this.truststorePassword != null) && (this.keystorePassword != null) && (this.keyPassword != null) && this.isSecure();
	}

}
