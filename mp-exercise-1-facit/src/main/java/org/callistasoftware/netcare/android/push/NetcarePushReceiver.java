package org.callistasoftware.netcare.android.push;

import org.callistasoftware.android.c2dm.C2DMReceiver;

public class NetcarePushReceiver extends C2DMReceiver {
	@Override
	public Class<?> getPushServiceClass() {
		return NetcarePushService.class;
	}
}
