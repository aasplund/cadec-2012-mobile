package org.callistasoftware.netcare.android.push;

import org.callistasoftware.android.c2dm.DefaultPushRegistrationHandler;
import org.callistasoftware.android.c2dm.PushHandler;
import org.callistasoftware.android.c2dm.PushRegistrationHandler;
import org.callistasoftware.android.c2dm.PushService;

import android.util.Log;

public class NetcarePushService extends PushService {

	private static final String TAG = NetcarePushService.class.getSimpleName();
	
	private String email;
	private String url;
	
	@Override
	public void onCreate() {
		
		Log.i(TAG, "Initializing Netcare Push Service");
		
		super.onCreate();
		
		this.email = "callista.push@gmail.com"; 
		this.url = "http://mt.callistasoftware.org:9090/netcare-server/system/c2dm";
		
		Log.d(TAG, "Email resolved to: " + this.email);
		Log.d(TAG, "Url resolved to: " + this.url);
	}
	
	@Override
	public PushRegistrationHandler registerPushRegistrationHandler() {
		Log.d(TAG, "Registering push registration handler");
		return new DefaultPushRegistrationHandler();
	}

	@Override
	public PushHandler registerPushHandler() {
		Log.d(TAG, "Registering push handler");
		return new PushNotificationCallback();
	}

	@Override
	public String registerPushEmail() {
		Log.d(TAG, "Registering push email address");
		return this.email;
	}

	@Override
	public String registerPushRegistrationUrl() {
		Log.d(TAG, "Registering push registration url");
		return this.url;
	}

}
