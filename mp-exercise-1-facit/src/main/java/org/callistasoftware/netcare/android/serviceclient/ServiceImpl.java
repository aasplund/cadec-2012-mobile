package org.callistasoftware.netcare.android.serviceclient;

import java.util.Collections;

import org.callistasoftware.netcare.android.dto.Ordination;
import org.callistasoftware.netcare.android.dto.Patient;
import org.callistasoftware.netcare.android.net.HttpClientConfiguration;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

import android.content.Context;
import android.util.Log;

public class ServiceImpl extends AbstractServiceClient implements ServiceInterface {
    
        private static final String TAG = ServiceImpl.class.getSimpleName();

	
	public ServiceImpl(final String host
			, final String baseUrl
			, final HttpClientConfiguration config
			, final Context context) {
		super(host, baseUrl, config, context);
	}
	
	@Override
	public ServiceResult<Patient> createNewUser(final Patient patient) {
		final String url = getBaseUrl() + "/user/new";
		Log.d(TAG, "Making call to: " + url);
		
		try {
			final RestTemplate rest = new RestTemplate(new HttpComponentsClientHttpRequestFactory(getHttpClient()));
			final ResponseEntity<Patient> result = rest.postForEntity(url, patient, Patient.class);
		
			if (result.getStatusCode() == HttpStatus.OK) {
				return ServiceResultImpl.newSuccessfulServiceResult(result.getBody());
			} else {
				return ServiceResultImpl.newFailedServiceResult("Bad return status. Code: " + result.getStatusCode());
			}
		} catch (Exception e) {
			e.printStackTrace();
			return ServiceResultImpl.newFailedServiceResult(e.getMessage());
		}
	}
	
	@Override
	public ServiceResult<Patient> login(String username, String password) {
		return this.doServiceCall("/patients/secure/login", Patient.class);
	}
	
	@Override
	public ServiceResult<Ordination[]> fetchOrdinations(Long patientId) {
	        return this.doServiceCall("/ordinations/list?patientId={patientId}", Ordination[].class, Collections.singletonMap("patientId", patientId));
	}
}
