package org.callistasoftware.netcare.android.serviceclient;

import org.callistasoftware.netcare.android.dto.Ordination;
import org.callistasoftware.netcare.android.dto.Patient;

/**
 * Defines a set of service calls
 * 
 * @author Marcus Krantz [marcus.krantz@callistaenterprise.se]
 *
 */
public interface ServiceInterface {
	
	/**
	 * Create a new patient
	 * @param fullName
	 * @param username
	 * @param password
	 * @return
	 */
	ServiceResult<Patient> createNewUser(final Patient patient);
	
	/**
	 * Login a user
	 * @param username
	 * @param password
	 * @param url
	 * @return
	 */
	ServiceResult<Patient> login(final String username, final String password);
	
	/**
	 * Fetches ordinations for a given patient
	 * @param patientId
	 * @param url
	 * @return
	 * @throws Exception
	 */
	ServiceResult<Ordination[]> fetchOrdinations(final Long patientId);
}
