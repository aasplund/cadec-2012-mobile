package org.callistasoftware.netcare.android.serviceclient;


/**
 * The result of a login operation
 * @author Marcus Krantz [marcus.krantz@callistaenterprise.se]
 *
 */
public interface ServiceResult<T> {

	/**
	 * True if the login was successful, false otherwise
	 * @return
	 */
	boolean isSuccess();
	
	/**
	 * Returns the error message if login was not successful,
	 * null otherwise
	 * @return
	 */
	String getErrorMessage();
	
	/**
	 * Data about the logged in user. Null if authentication was not successful
	 * @return
	 */
	T getData();
}
