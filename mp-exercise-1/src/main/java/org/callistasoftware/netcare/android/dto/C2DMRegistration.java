package org.callistasoftware.netcare.android.dto;

public interface C2DMRegistration {

	/**
	 * Get the account id of the target user
	 * that will register for push notifications
	 * @return
	 */
	Long getAccountId();
	
	/**
	 * Get the registration id that Google provides us
	 * with
	 * @return
	 */
	String getRegistrationId();
}
