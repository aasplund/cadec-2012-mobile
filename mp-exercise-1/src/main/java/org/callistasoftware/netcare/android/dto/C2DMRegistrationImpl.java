package org.callistasoftware.netcare.android.dto;

public class C2DMRegistrationImpl implements C2DMRegistration {

	private Long accountId;
	private String registrationId;
	
	private C2DMRegistrationImpl(final Long accountId, final String registrationId) {
		this.accountId = accountId;
		this.registrationId = registrationId;
	}
	
	public static C2DMRegistration create(final Long accountId, final String registrationId) {
		return new C2DMRegistrationImpl(accountId, registrationId);
	}
	
	@Override
	public Long getAccountId() {
		return this.accountId;
	}
	
	@Override
	public String getRegistrationId() {
		return this.registrationId;
	}

}
