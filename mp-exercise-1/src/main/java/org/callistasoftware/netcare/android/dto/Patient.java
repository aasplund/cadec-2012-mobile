package org.callistasoftware.netcare.android.dto;

import org.codehaus.jackson.annotate.JsonProperty;

/**
 * Implementation of the {@link Patient} interface
 * 
 * @author Marcus Krantz [marcus.krantz@callistaenterprise.se]
 *
 */
public class Patient {

	@JsonProperty
	private Long id;
	
	@JsonProperty
	private String name;
	
	@JsonProperty
	private String civicRegistrationNumber;
	
	@JsonProperty
	private String securityCode;
	
	public Long getId() {
		return this.id;
	}
	
	public void setId(final Long id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}
	
	public void setName(final String name) {
		this.name = name;
	}

	public String getCivicRegistrationNumber() {
		return this.civicRegistrationNumber;
	}
	
	public void setCivicRegistrationNumber(final String civicRegistrationNumber) {
		this.civicRegistrationNumber = civicRegistrationNumber;
	}
	
	public String getSecurityCode() {
		return this.securityCode;
	}
	
	public void setSecurityCode(final String securityCode) {
		this.securityCode = securityCode;
	}
	
	@Override
	public String toString() {
		final StringBuilder builder = new StringBuilder();
		builder.append("=== Patient ===\n");
		builder.append("Name: ").append(this.name).append("\n");
		builder.append("Crn: ").append(this.civicRegistrationNumber).append("\n");
		builder.append("===");
		
		return builder.toString();
	}
}
