package org.callistasoftware.netcare.android.net;

import java.io.InputStream;
import java.security.KeyStore;

import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.SingleClientConnManager;
import org.callistasoftware.netcare.android.R;

import android.content.Context;
import android.util.Log;

/**
 * A custom http client implementation that uses the sepcified
 * {@link HttpClientConfiguration}
 * 
 * @author Marcus Krantz [marcus.krantz@callistaenterprise.se]
 *
 */
public class CustomHttpClient extends DefaultHttpClient {

	private static final String TAG = CustomHttpClient.class.getSimpleName();
	
	private Context mContext;
	private HttpClientConfiguration config;
	
	public void configure(final HttpClientConfiguration config, final Context context) {
		this.config = config;
		this.mContext = context;
		
		Log.d(TAG, "Checking whether to configure basic authentication: " + this.config.isBasicAuthentication());
		if (this.config.isBasicAuthentication()) {
			this.configureBasicAuthentication();
		}
	}
	
	public HttpClientConfiguration getConfiguration() {
		return this.config;
	}
	
	@Override
	protected ClientConnectionManager createClientConnectionManager() {
		Log.d(TAG, "Creating client connection manager");
		
		final SchemeRegistry registry = new SchemeRegistry();
		
		if (this.config.isSecure()) {
			Log.d(TAG, "Adding https scheme for port " + this.config.getSecurePort());
			/*
			 * ÖVNING 1a
			 * 
			 * 
			 * Registrera ett schema för säker anslutning:
			 * registry.register(new Scheme(https, this.createSSLSocketFactory(), securePort))
			 * 
			 * this.createSSLSocketFactory() är en metod som ni nu skall implementera. Tanken
			 * är att ni i denna metod skall returnera en SSLSocketFactory som pratar säkert.
			 * 
			 */
		}
		
		return new SingleClientConnManager(getParams(), registry);
	}
	
	private SSLSocketFactory createSSLSocketFactory() {
		
		Log.d(TAG, "Creating SSL socket factory");
		
		/*
		 * Övning 1a forts.
		 * Det första vi behöver göra är att ladda våran truststore. I denna truststore finns servern's
		 * certifikat importerat.
		 * 
		 * Metoden loadStore() i denna klass laddar en truststore. Argumenten du behöver skicka in finns
		 * i HttpClientConfiguration instansen. Se metoderna getTrusstoreResourceId(), getTruststorePassword()
		 * 
		 * Store type argumentet skall vara 'BKS' eftersom truststoren har skapats med hjälp
		 * av BouncyCastle's provider.
		 * 
		 */
		final KeyStore truststore;
		
		/*
		 * Fortsätt nu med att implementera loadStore()-metoden och ladda truststoren. Återkom sedan hit.
		 */
		
		/*
		 * ÖVNING 1a forts.
		 * Instansiera en ny SSLSocketFactory:n och skicka med vår inladdade trust store.
		 * 
		 * Då hostnamnet i servern's certifikat inte stämmer överens med dess riktiga
		 * adress behöver vi tala om att vi skall tillåta alla hostnamn mha SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER
		 * 
		 * Om det går fel att skapa SSLSocketFactory:n kommer vi verkligen inte vidare i applikationen.
		 * Därför ska felet loggas och kasta sedan ett RuntimeException som gör att vår
		 * applikation kommer att krasha.
		 * 
		 * I normala fall hade vi velat ta hand om det här felet och meddelat användaren att
		 * det inte gick att skapa vår säkra anslutning, men sannolikheten att det är
		 * ett programmatiskt/konfiguration fel är stor så därför nöjer vi oss
		 * med att krasha.
		 */
		return null;
	}
	
	private KeyStore loadStore(final int store, final String storePass, final String storeType) {
		/*
		 * ÖVNING 1a.
		 * Det vi skall göra nu är att ladda våran truststore. Dock så måste vi
		 * först måste vi använda KeyStore.getInstance() metoden för att få tag
		 * i vår Keystore som vi sedan ska ladda våran truststore i.
		 * 
		 * Vi måste ange vilken typ av keystore som skall returneras genom att ange
		 * "BKS". storeType-variabeln bör ha detta värde så denna skall fungera
		 * utmärkt att skickan in till getInstance()-metoden.
		 * 
		 * Därefter behöver vi ladda trustoren till våran keystore genom att använda
		 * metoden load(). Den här metoden tar en InputStream till truststoren samt
		 * lösenordet till truststoren. Tips: Lösenord använd String-klassens 
		 * toCharArray()-metod
		 * 
		 * Våran truststore finns i res/raw/-katalogen. Vid kompilering genererar Android en
		 * klass R som ger tillgång till alla statiska resurser via statiska metoder. För att
		 * få en InputStream till våran truststore används applikations contextet som finns i
		 * this.mContext
		 * 
		 * Exempel:
		 * final InputStream is = this.mContext.getResources().openRawResource(<resurs_id>);
		 * 
		 * <resurs_id> är id:t på truststore resursen. Android tilldelar oss detta id:t vid
		 * kompilering och finns tillgängligt i this.config.getTruststoreResourceId().
		 * 
		 * OBS! tänk på att stänga strömmen efter laddning av truststoren
		 * OBS! Felhantering. Fånga eventuella exceptions, logga dem. Kasta sedan ett
		 * runtime-exception och krasha applikationen.
		 * 
		 */
		return null;
	}
	
	/**
	 * Configure basic authentication
	 */
	private void configureBasicAuthentication() {
		Log.d(TAG, "Configuring basic authentication...");
		
		/*
		 * ÖVNING 1b - BASIC AUTHENTICATION
		 * I denna övning kommer vi att lägga på stöd för basic authentication.
		 * Det här innebär att vid varje anrop mot våra tjänster kommer vi att
		 * "haka" på användarnamn och lösenord för användaren.
		 * 
		 * Användarnamn och lösenord finns tillgängligt i våran
		 * HttpClientConfiguration.
		 * 
		 * Det man gör när man "hakar" på Basic authentication är att man lägger till en
		 * "request interceptor" på sin http client.
		 * http://hc.apache.org/httpcomponents-core-ga/httpcore/apidocs/org/apache/http/protocol/BasicHttpProcessor.html#addRequestInterceptor(org.apache.http.HttpRequestInterceptor, int)
		 * 
		 * Som första argument till denna metod skickar man en RequestInterceptor som kommer att exekvera precis
		 * innan anropet går iväg mot servern. Som andra argument skickar man 0 som kommer att lägga våran
		 * interceptor först i listan med andra interceptors som kommer att exekvera.
		 * 
		 * I den plockar man ut ett AuthState ifrån HttpContext som kommer som argument. AuthState-attributet
		 * på HttpContext:et hämtas med hjälp av context.getAttribute(ClientContext.TARGET_AUTH_STATE);
		 * 
		 * På AuthState objektet behöver sätta vilket autentiserings schema som man vill använda,
		 * i vårat fall en instans av BasicScheme. Därefter behöver man sätta credentials som i vårat fall
		 * ska vara en instans av UsernamePasswordCredentials
		 * 
		 */
	}
}
