package org.callistasoftware.netcare.android.net;

/**
 * Configuration interface. Defines methods for configuring a
 * HttpClient
 * 
 * @author Marcus Krantz [marcus.krantz@callistaenterprise.se]
 *
 */
public interface HttpClientConfiguration {

	/**
	 * Set credentials to use for basic authentication
	 * @param username
	 * @param password
	 */
	void setBasicAuthenticationCredentials(final String username, final String password);
	
	/**
	 * The username to use in basic authentication
	 * @return
	 */
	String getUsername();
	
	/**
	 * The password to use in basic authentication
	 * @return
	 */
	String getPassword();
	
	/**
	 * Set the server port that runs TLS/SSL
	 * @param port
	 */
	void setSecurePort(final int port);
	
	/**
	 * The secure server port
	 * @return
	 */
	int getSecurePort();
	
	/**
	 * Set the server port that runs plain http
	 * @param port
	 */
	void setUnsecurePort(final int port);
	
	/**
	 * The unsecure server port
	 * @return
	 */
	int getUnsecurePort();
	
	/**
	 * Set truststore configuration.
	 * @param truststoreResourceId
	 * @param truststorePassword
	 */
	void setTruststore(final int truststoreResourceId, final String truststorePassword);
	
	/**
	 * The truststore's resource id
	 * @return
	 */
	int getTruststoreResourceId();
	
	/**
	 * The truststore's password
	 * @return
	 */
	String getTruststorePassword();
	
	/**
	 * Set keystore configuration. Used in mutual authentication scenarios
	 * @param keystoreResourceId
	 * @param keystorePassword
	 * @param keyPassword
	 */
	void setKeystore(final int keystoreResourceId, final String keystorePassword, final String keyPassword);
	
	/**
	 * The keystore's resource id
	 * @return
	 */
	int getKeystoreResourceId();
	
	/**
	 * The keystore's password
	 * @return
	 */
	String getKeystorePassword();
	
	/**
	 * The key's password
	 * @return
	 */
	String getKeyPassword();
	
	/**
	 * Whether the configuration supports basic authentication
	 * @return
	 */
	boolean isBasicAuthentication();
	
	/**
	 * Whether the configuration supports secure communication
	 * @return
	 */
	boolean isSecure();
	
	/**
	 * Whether the configuration supports mutual authentication
	 * @return
	 */
	boolean isMutualAuthentication();
}
