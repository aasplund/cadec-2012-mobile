package org.callistasoftware.netcare.android.push;

import org.callistasoftware.android.c2dm.C2DMReceiver;
import org.callistasoftware.android.c2dm.PushHandler;
import org.callistasoftware.netcare.android.dto.C2DMRegistrationImpl;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

/**
 * A sample callback for push notifications on Android using
 * Googles C2DM (Cloud to device messaging framework)
 * 
 * @author Marcus Krantz [marcus.krantz@callistaenterprise.se]
 *
 */
public class PushNotificationCallback implements PushHandler {

	private static final String TAG = PushNotificationCallback.class.getSimpleName();
	
	@Override
	public void handlePushEvent(Context context, Intent intent) {
		Log.d(TAG, "Message intent received.");
		
		final NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
		
		final String subject = intent.getExtras().getString("title");
		final String message = intent.getExtras().getString("message");
		final String when = intent.getExtras().getString("timestamp");
		
		final Intent notificationIntent = new Intent(context, C2DMReceiver.class);
		final PendingIntent contentIntent = PendingIntent.getActivity(context.getApplicationContext(), 0, notificationIntent, 0);
		
		final Notification notification = new Notification(android.R.drawable.btn_star_big_on, message, Long.valueOf(when));
		notification.setLatestEventInfo(context.getApplicationContext(), subject, message, contentIntent);
		notificationManager.notify(1, notification);
		
		return;
	}

	@Override
	public void handlePushRegistration(Context context, Intent intent, final String registrationId, final String url) {
		RestTemplate rest = new RestTemplate();
		
		Log.d(TAG, "Sending registraion id to server to enable push");
		try {
			
			final long userId = context.getSharedPreferences("NETCARE", 0).getLong("userid", -1L);
			rest.put(url, C2DMRegistrationImpl.create(userId, registrationId));
		} catch (final ResourceAccessException e) {
			Log.e(TAG, "Could not register for push. Reason: " + e.getMessage());
		}
	}

	@Override
	public void handlePushUnregistration(Context context, Intent intent) {
		throw new UnsupportedOperationException("Not yet implemented");
	}
}
