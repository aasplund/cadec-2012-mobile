package org.callistasoftware.netcare.android;

import java.util.Arrays;

import org.callistasoftware.netcare.android.adapter.OrdinationListAdapter;
import org.callistasoftware.netcare.android.dto.Ordination;
import org.callistasoftware.netcare.android.net.HttpClientConfiguration;
import org.callistasoftware.netcare.android.net.HttpConfigurationFactory;
import org.callistasoftware.netcare.android.serviceclient.ServiceFactory;
import org.callistasoftware.netcare.android.serviceclient.ServiceInterface;
import org.callistasoftware.netcare.android.serviceclient.ServiceResult;

import android.app.ListActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ListAdapter;
import android.widget.Toast;

public class OrdinationListActivity extends ListActivity {

	private static final String TAG = OrdinationListActivity.class.getSimpleName();
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            
            Log.i(TAG, "Displaying ordination list");
            setContentView(R.layout.ordination_list);
            Log.i(TAG, "setcontentview");
            
            final String username = "123456789004";
            final String pincode = "0000";
            final Long patientId = 4L;
            
            final HttpClientConfiguration config = HttpConfigurationFactory.newSecureConfigurationWithBasicAuthentication(9443, username, pincode, R.raw.clienttruststore, "password");
            final ServiceInterface service = ServiceFactory.createNewServiceInterface(getApplicationContext(), config);
            Log.d(TAG, "New service interface called");
            
            final ServiceResult<Ordination[]> response = service.fetchOrdinations(patientId);
            Log.d(TAG, "Got result from service. Success:" + response.isSuccess());
            if (response.isSuccess()) {
                    Log.d(TAG, "Ordinations size: " + response.getData().length);
                    
                    final ListAdapter adapter = new OrdinationListAdapter(getApplicationContext(), Arrays.asList(response.getData()));
                    setListAdapter(adapter);
            } else {
                    Log.e(TAG, "Service call failed. Reason: " + response.getErrorMessage());
                    Toast.makeText(getApplicationContext(), response.getErrorMessage(), 5000);
            }
	}
}
