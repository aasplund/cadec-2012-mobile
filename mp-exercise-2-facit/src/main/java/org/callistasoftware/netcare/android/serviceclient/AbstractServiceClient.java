package org.callistasoftware.netcare.android.serviceclient;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.util.Map;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.callistasoftware.netcare.android.net.CustomHttpClient;
import org.callistasoftware.netcare.android.net.HttpClientConfiguration;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.http.HttpStatus;
import org.springframework.web.util.UriTemplate;

import android.content.Context;
import android.util.Log;

public abstract class AbstractServiceClient {

    private static final String TAG = AbstractServiceClient.class.getSimpleName();
    private String baseUrl;
    private CustomHttpClient httpClient;

    public AbstractServiceClient(final String host, final String baseUrl, final HttpClientConfiguration config,
            final Context context) {

        this.httpClient = new CustomHttpClient();
        this.httpClient.configure(config, context);
        this.baseUrl = constructBaseUrl(config, host, baseUrl);
    }

    protected <T> ServiceResult<T> doServiceCall(final String url, final Class<T> returnClass) {
        Log.d(TAG, "Making call to: " + getBaseUrl() + url);
        final HttpGet request = new HttpGet(getBaseUrl() + url);

        try {
            final HttpResponse response = this.httpClient.execute(request);

            if (response.getStatusLine().getStatusCode() == HttpStatus.OK.value()) {
                final T parsedData = this.parseServiceResponse(response, returnClass);
                return ServiceResultImpl.newSuccessfulServiceResult(parsedData);
            } else {
                return ServiceResultImpl.newFailedServiceResult("Connection error. Response code: "
                        + response.getStatusLine().getStatusCode());
            }
        } catch (final Exception e) {
            e.printStackTrace();
            return ServiceResultImpl.newFailedServiceResult("Error when communicating with the server. Reason: "
                    + e.getMessage());
        }
    }

    protected <T> ServiceResult<T> doServiceCall(final String url, final Class<T> returnClass,
            Map<String, ?> urlVariables) {
        URI expanded = new UriTemplate(url).expand(urlVariables);
        return doServiceCall(expanded.toString(), returnClass);
    }

    protected <T> T parseServiceResponse(final HttpResponse response, final Class<T> returnClass)
            throws JsonParseException, JsonMappingException, IllegalStateException, IOException {
        final ObjectMapper mapper = new ObjectMapper();
        InputStream content = response.getEntity().getContent();
        if (returnClass.getName().equalsIgnoreCase("java.lang.String")) {
            String line = "";
            StringBuilder sb = new StringBuilder();
            BufferedReader reader = new BufferedReader(new InputStreamReader(content, "UTF-8"));
            while ((line = reader.readLine()) != null) {
                sb.append(line);
            }
            return (T) sb.toString();
        }
        return mapper.readValue(content, returnClass);
    }

    protected String getBaseUrl() {
        return baseUrl;
    }

    protected CustomHttpClient getHttpClient() {
        return httpClient;
    }

    private static String constructBaseUrl(final HttpClientConfiguration httpClientConfig, final String host,
            final String baseUrl) {
        final StringBuilder builder = new StringBuilder();
        final boolean secure = httpClientConfig.isSecure();

        if (secure) {
            builder.append("https://").append(host).append(":").append(httpClientConfig.getSecurePort());
        } else {
            builder.append("http://").append(host).append(":").append(httpClientConfig.getUnsecurePort());
        }

        builder.append(baseUrl);

        Log.d(TAG, "Constructed base url for call: " + builder.toString());

        return builder.toString();
    }
}
