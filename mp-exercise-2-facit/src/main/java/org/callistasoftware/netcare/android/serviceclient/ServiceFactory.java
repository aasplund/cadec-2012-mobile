package org.callistasoftware.netcare.android.serviceclient;

import org.callistasoftware.netcare.android.net.HttpClientConfiguration;

import android.content.Context;

public class ServiceFactory {

    public static ServiceInterface createNewServiceInterface(final Context context, final HttpClientConfiguration config) {
        return new ServiceImpl("mt.callistasoftware.org", "/netcare-server", config, context);
    }
}
