package org.callistasoftware.netcare.android.serviceclient;

/**
 * Implementation of {@link ServiceResult}
 * 
 * @author Marcus Krantz [marcus.krantz@callistaenterprise.se]
 * 
 */
public class ServiceResultImpl<T> implements ServiceResult<T> {

    private boolean success;
    private String errorMessage;
    private T data;

    private ServiceResultImpl(final boolean success, final String errorMessage, final T data) {
        this.success = success;
        this.errorMessage = errorMessage;
        this.data = data;
    }

    public static <T> ServiceResult<T> newSuccessfulServiceResult(final T data) {
        return new ServiceResultImpl<T>(true, null, data);
    }

    public static <T> ServiceResult<T> newFailedServiceResult(final String errorMessage) {
        return new ServiceResultImpl<T>(false, errorMessage, null);
    }

    @Override
    public boolean isSuccess() {
        return this.success;
    }

    @Override
    public String getErrorMessage() {
        if (this.isSuccess() && this.errorMessage != null) {
            throw new IllegalStateException("Claims to be successful but we have an error message");
        }

        return this.errorMessage;
    }

    @Override
    public T getData() {
        if (!this.isSuccess() && this.data != null) {
            throw new IllegalStateException("Claims to be unsuccessful but we have response data.");
        }

        return this.data;
    }

}
