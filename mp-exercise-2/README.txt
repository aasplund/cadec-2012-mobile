Android mobile reference application for Cadec Tutorial 2012.

--Install maven 2.2.1--

--Install android-sdk--

export ANDROID_HOME=/Users/hansthunberg/Applications/android-sdk-mac_x86
export PATH=$PATH:$ANDROID_HOME/tools
export PATH=$PATH:$ANDROID_HOME/platform-tools

Create virtual device for API level 8 or newer, current code is tested on API level 8.

--Android maven plugin--

To test the app in emulator:
run mvn clean install
run mvn android:emulator-start
run mvn android:deploy
to undeploy run mvn android:undeploy

To start and stop emulator using maven:
mvn android:emulator-start
mvn android:emulator-stop

To se console output:
android_home/platform-tools $> adb logcat





