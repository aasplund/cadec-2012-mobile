package org.callistasoftware.netcare.android;

import org.callistasoftware.netcare.android.net.HttpClientConfiguration;
import org.callistasoftware.netcare.android.net.HttpConfigurationFactory;
import org.callistasoftware.netcare.android.serviceclient.ServiceFactory;
import org.callistasoftware.netcare.android.serviceclient.ServiceInterface;
import org.callistasoftware.netcare.android.serviceclient.ServiceResult;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

/**
 * 
 * Handle the BankId login for the netcare application.
 * 
 */
public class BankIdLoginActivity extends Activity {

    private static final String TAG = BankIdLoginActivity.class.getSimpleName();

    private EditText ffPersonnummer;
    private Button btnLogin;
    private String orderRef;
    private ServiceInterface bankidService;

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i(TAG, "Display BankId Login");

        final HttpClientConfiguration config = HttpConfigurationFactory.newSecureConfiguration(9443, R.raw.clienttruststore, "password");
        bankidService = ServiceFactory.createNewServiceInterface(getApplicationContext(), config);

        setContentView(R.layout.bankid_login);
        ffPersonnummer = (EditText) findViewById(R.id.personnummer);
        btnLogin = (Button) findViewById(R.id.login_button);

        btnLogin.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                final String personnummer = ffPersonnummer.getText().toString();
                new ServiceCallTask<String>(BankIdLoginActivity.this, new ServiceCallback<String>() {

                    @Override
                    public String getProgressMessage() {
                        return getApplicationContext().getString(R.string.bankid_progress);
                    }

                    @Override
                    public ServiceResult<String> doCall() {
                        Log.d(TAG, "Authenticating user: " + personnummer);
                        /* Övning 2b-1
                         * 
                         * 
                         * Begär legitimering för användaren genom att använda bankidService.
                         */
                        return null;
                    }

                    @Override
                    public void onSuccess(ServiceResult<String> result) {
                        /* Övning 2b-2
                         * 
                         * 
                         * Trigga ett intent för att öppna BankID-appen. Se avsnitt 7.2.1.2
                         * i api-dokumentationen.
                         * 
                         * OBS! Notera att redirect-parametern i url-schemat måste stämma
                         * överens med schemat i intent-filtret i AndroidManifest.xml
                         */
                    }
                }).execute();
            }
        });
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.i(TAG, "Restart BankIdLoginActivity");
        new ServiceCallTask<Boolean>(BankIdLoginActivity.this, new ServiceCallback<Boolean>() {

            @Override
            public String getProgressMessage() {
                return getApplicationContext().getString(R.string.login_progress);
            }

            @Override
            public ServiceResult<Boolean> doCall() {
                Log.d(TAG, "Fetch authentication status for orderRef: " + orderRef);
                /*
                 * Övning 2b-4
                 * 
                 * 
                 * Verifiera legitimeringen genom att använda bankidService. 
                 */
                return null;
            }

            @Override
            public void onSuccess(ServiceResult<Boolean> result) {
                /* Övning 2b-5
                 * 
                 * 
                 * Om legitimeringen gick bra skall vi visa ordinationerna via
                 * OrdinationListActivity.
                 * Starta en ny activity mha av startActivity(Intent intent).
                 * 
                 */
            }
        }).execute();
    }
}
