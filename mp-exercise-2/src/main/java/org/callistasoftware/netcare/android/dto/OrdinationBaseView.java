package org.callistasoftware.netcare.android.dto;

import java.util.Date;

public interface OrdinationBaseView {

	Long getId();
	
	String getTitle();
	
	String getDescription();
	
	Date getDueDate();
}
