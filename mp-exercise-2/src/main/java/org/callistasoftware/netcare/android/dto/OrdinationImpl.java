package org.callistasoftware.netcare.android.dto;

import java.util.Date;

public class OrdinationImpl implements OrdinationBaseView {

	private Long id;
	private String title;
	private String description;
	private Date dueDate;
	
	
	@Override
	public Long getId() {
		return this.id;
	}
	
	void setId(final Long id) {
		this.id = id;
	}

	@Override
	public String getTitle() {
		return this.title;
	}
	
	void setTitle(final String title) {
		this.title = title;
	}

	@Override
	public String getDescription() {
		return this.description;
	}
	
	void setDescription(final String description) {
		this.description = description;
	}

	@Override
	public Date getDueDate() {
		return this.dueDate;
	}
	
	void setDueDate(final Date d) {
		this.dueDate = d;
	}

}
