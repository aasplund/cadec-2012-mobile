package org.callistasoftware.netcare.android.net;

public class HttpConfigurationFactory {

    public static HttpClientConfiguration newPlainConfiguration(final int port) {
        final HttpClientConfigurationImpl conf = new HttpClientConfigurationImpl();
        conf.setUnsecurePort(port);

        return conf;
    }

    public static HttpClientConfiguration newPlainConfigurationWithBasicAuthentication(final int port,
            final String username, final String password) {
        final HttpClientConfigurationImpl conf = new HttpClientConfigurationImpl();
        conf.setUnsecurePort(port);
        conf.setBasicAuthenticationCredentials(username, password);

        return conf;
    }

    public static HttpClientConfiguration newSecureConfiguration(final int port, final int truststoreResourceId,
            final String truststorePassword) {
        final HttpClientConfigurationImpl conf = new HttpClientConfigurationImpl();
        conf.setSecurePort(port);
        conf.setTruststore(truststoreResourceId, truststorePassword);

        return conf;
    }

    public static HttpClientConfiguration newSecureConfigurationWithBasicAuthentication(final int port,
            final String username, final String password, final int truststoreResourceId,
            final String truststorePassword) {
        final HttpClientConfigurationImpl conf = new HttpClientConfigurationImpl();
        conf.setBasicAuthenticationCredentials(username, password);
        conf.setTruststore(truststoreResourceId, truststorePassword);
        conf.setSecurePort(port);

        return conf;
    }

    public static HttpClientConfiguration newSecureMutualConfigurationWithBasicAuthentication(final int port,
            final String username, final String password, final int truststoreResourceId,
            final String truststorePassword, final int keystoreResourceId, final String keystorePassword,
            final String keyPassword) {
        final HttpClientConfigurationImpl conf = new HttpClientConfigurationImpl();
        conf.setSecurePort(port);
        conf.setBasicAuthenticationCredentials(username, password);
        conf.setKeystore(keystoreResourceId, keystorePassword, keyPassword);
        conf.setTruststore(truststoreResourceId, truststorePassword);

        return conf;
    }
}
