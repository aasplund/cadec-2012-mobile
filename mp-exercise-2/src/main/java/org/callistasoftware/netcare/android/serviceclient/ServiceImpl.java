package org.callistasoftware.netcare.android.serviceclient;

import java.util.Collections;

import org.callistasoftware.netcare.android.dto.Ordination;
import org.callistasoftware.netcare.android.net.HttpClientConfiguration;

import android.content.Context;

public class ServiceImpl extends AbstractServiceClient implements ServiceInterface {

    public ServiceImpl(final String host, final String baseUrl, final HttpClientConfiguration config,
            final Context context) {
        super(host, baseUrl, config, context);
    }

    @Override
    public ServiceResult<Ordination[]> fetchOrdinations(Long patientId) {
        return this.doServiceCall("/ordinations/list?patientId={patientId}", Ordination[].class, Collections.singletonMap("patientId", patientId));
    }

    @Override
    public ServiceResult<String> authenticate(String personnummer) {
        return doServiceCall("/bankid/legitimera/{personnummer}", String.class, Collections.singletonMap("personnummer", personnummer));
    }

    @Override
    public ServiceResult<Boolean> collect(String orderRef) {
        return doServiceCall("/bankid/collect/{orderRef}", Boolean.class, Collections.singletonMap("orderRef", orderRef));
    }
}
