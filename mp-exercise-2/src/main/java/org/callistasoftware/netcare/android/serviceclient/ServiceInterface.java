package org.callistasoftware.netcare.android.serviceclient;

import org.callistasoftware.netcare.android.dto.Ordination;

/**
 * Defines a set of service calls
 * 
 * @author Marcus Krantz [marcus.krantz@callistaenterprise.se]
 * 
 */
public interface ServiceInterface {

    /**
     * Fetches ordinations for a given patient
     * 
     * @param patientId
     * @param url
     * @return
     * @throws Exception
     */
    ServiceResult<Ordination[]> fetchOrdinations(final Long patientId);

    ServiceResult<String> authenticate(String personnummer);

    ServiceResult<Boolean> collect(String orderRef);
}
