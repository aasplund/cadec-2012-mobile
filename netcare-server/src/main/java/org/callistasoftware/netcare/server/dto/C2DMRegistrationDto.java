package org.callistasoftware.netcare.server.dto;

public class C2DMRegistrationDto {

	private Long accountId;
	private String registrationId;
	
	public Long getAccountId() {
		return this.accountId;
	}
	
	void setAccountId(final Long accountId) {
		this.accountId = accountId;
	}
	
	public String getRegistrationId() {
		return this.registrationId;
	}
	
	void setRegistrationId(final String registrationId) {
		this.registrationId = registrationId;
	}
}
