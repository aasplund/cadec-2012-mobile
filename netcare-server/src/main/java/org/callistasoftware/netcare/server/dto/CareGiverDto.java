package org.callistasoftware.netcare.server.dto;

import org.callistasoftware.netcare.server.model.CareGiver;

public class CareGiverDto {

	private Long id;
	
	private String name;
	
	private String hsaId;
	
	CareGiverDto() {}
	
	public static CareGiverDto create(final String name) {
		final CareGiverDto dto = new CareGiverDto();
		dto.setId(-1L);
		dto.setName(name);
		
		return dto;
	}
	
	public static CareGiverDto createFromEntity(final CareGiver careGiver) {
		return DtoFactory.createDtoFromEntity(careGiver);
	}

	public Long getId() {
		return id;
	}

	void setId(Long id) {
		this.id = id;
	}

	public String getHsaId() {
		return hsaId;
	}
	
	public String getName() {
		return name;
	}

	void setName(String name) {
		this.name = name;
	}
}
