package org.callistasoftware.netcare.server.dto;

import com.bankid.rpservice.v1_0_0.types.CollectResponseType;

public class CollectResponseTypeDto {

    private ProgressStatusTypeDto progressStatus;
    private String signature;
    
    public static final CollectResponseTypeDto EMPTY_RESPONSE = new CollectResponseTypeDto();

    CollectResponseTypeDto() {
    }

    public static CollectResponseTypeDto createFromEntity(final CollectResponseType response) {
        return DtoFactory.createDtoFromEntity(response);
    }
    
    public void setProgressStatus(ProgressStatusTypeDto progressStatus) {
        this.progressStatus = progressStatus;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    public ProgressStatusTypeDto getProgressStatus() {
        return progressStatus;
    }

    public String getSignature() {
        return signature;
    }

}
