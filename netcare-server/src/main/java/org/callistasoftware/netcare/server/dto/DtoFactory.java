package org.callistasoftware.netcare.server.dto;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.bankid.rpservice.v1_0_0.types.ProgressStatusType;

/**
 * Creates data transfer objects.
 * 
 * @author Peter
 */
public class DtoFactory {

    public static final String DATA_SUFFIX = "Dto";
    private static final Package PKG = DtoFactory.class.getPackage();
    private static final Map<Class<?>, Class<?>> classMap = new ConcurrentHashMap<Class<?>, Class<?>>();
    private static final Map<Field, String> methodMap = new ConcurrentHashMap<Field, String>();

    /**
     * Creates a data transfer object of appropriate type, by naming convention.
     * <p>
     * 
     * Naming: [this_package].[class_name]Dto
     * <p>
     * 
     * All fields defined in the data object are are copied from the source object, and a matching method must exist in
     * the source object.
     * 
     * @param entity
     *            the source model object, can be of any type or package.
     * @return the corresponding data transfer object, must be declared in this package namespace.
     */
    @SuppressWarnings("unchecked")
    public static <T> T createDtoFromEntity(Object entity) {
        try {
            return (T) copy(getDtoClassFor(entity.getClass()), entity);
        } catch (Exception e) {
            throw new IllegalStateException(e);
        }
    }

    //
    private static <T> T copy(Class<T> targetClass, Object source) throws Exception {
        T target = null;
        if (targetClass.isEnum()) {
            target = copyEnum(targetClass, source);
        } else {
            target = copyClass(targetClass, source);
        }
        return target;
    }

    private static <T> T copyEnum(Class<T> targetClass, Object source) throws Exception {
        Enum<?> sourceEnum = (Enum<?>) source;
        List<T> enumConstants = Arrays.asList(targetClass.getEnumConstants());
        for (T constant : enumConstants) {
            Enum<?> targetEnum = (Enum<?>) constant;
            if (targetEnum.name().equals(sourceEnum.name())) {
                return constant;
            }
        }
        throw new Exception("Unknown Enum constant: " + sourceEnum);
    }

    private static <T> T copyClass(Class<T> targetClass, Object source) throws Exception {
        T target;
        target = targetClass.newInstance();
        for (Field f : target.getClass().getDeclaredFields()) {
            if (classHasMethod(source.getClass(), getMethodNameFor(f))) {
                f.setAccessible(true);
                f.set(target, get(f, source));
            }
        }
        return target;
    }

    //
    private static Object get(Field field, Object source) throws Exception {
        String mName = getMethodNameFor(field);
        return marshal(field.getType(), source.getClass().getMethod(mName).invoke(source));
    }

    private static boolean classHasMethod(Class<?> sourceClass, String methodName) {
        try {
            sourceClass.getMethod(methodName);
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    // XXX: shallow support only.
    private static Object marshal(Class<?> returnType, Object value) throws Exception {
        if (value == null) {
            return null;
        }
        if (returnType.isAssignableFrom(List.class)) {
            LinkedList<Object> list = new LinkedList<Object>();
            for (Object v : (List<?>) value) {
                list.add(createDtoFromEntity(v));
            }
            return list;
        } else if (returnType.getPackage().equals(PKG)) {
            return copy(returnType, value);
        }
        return value;
    }

    //
    private static Class<?> getDtoClassFor(Class<?> source) throws ClassNotFoundException {
        Class<?> t = classMap.get(source);
        if (t == null) {
            t = Class.forName(PKG.getName() + "." + source.getSimpleName() + DATA_SUFFIX);
            classMap.put(source, t);
        }
        return t;
    }

    //
    private static String getMethodNameFor(Field field) {
        String m = methodMap.get(field);
        if (m == null) {
            String fName = field.getName();
            m = "get" + Character.toUpperCase(fName.charAt(0)) + ((fName.length() > 1) ? fName.substring(1) : "");
            methodMap.put(field, m);
        }
        return m;
    }
}
