package org.callistasoftware.netcare.server.dto;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import org.callistasoftware.netcare.server.model.Ordination;
import org.codehaus.jackson.annotate.JsonIgnore;


public class OrdinationDto {

	private Long id;
	
	private String title;
	private String description;
	
	private String reported;
	
	private Date due;
	private Date reminder;
	
	@JsonIgnore
	private PatientDto patient;
	
	@JsonIgnore
	private List<CareGiverDto> careGivers;
	
	OrdinationDto() {}
	
	public static OrdinationDto create(final String title, final PatientDto patient) {
		final OrdinationDto dto = new OrdinationDto();
		dto.setId(-1L);
		dto.setTitle(title);
		dto.setPatient(patient);
		dto.setCareGivers(new LinkedList<CareGiverDto>());
		
		return dto;
	}
	
	public static OrdinationDto createFromEntity(final Ordination ordination) {
		return DtoFactory.createDtoFromEntity(ordination);
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	void setDescription(String description) {
		this.description = description;
	}

	public PatientDto getPatient() {
		return patient;
	}

	void setPatient(PatientDto patient) {
		this.patient = patient;
	}

	public List<CareGiverDto> getCareGivers() {
		return careGivers;
	}

	void setCareGivers(List<CareGiverDto> careGivers) {
		this.careGivers = careGivers;
	}

	public String getReported() {
		return reported;
	}

	void setReported(String reported) {
		this.reported = reported;
	}

	public Date getDue() {
		return due;
	}

	void setDue(Date due) {
		this.due = due;
	}

	public Date getReminder() {
		return reminder;
	}

	void setReminder(Date reminder) {
		this.reminder = reminder;
	}
}
