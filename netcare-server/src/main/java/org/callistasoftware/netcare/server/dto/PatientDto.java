package org.callistasoftware.netcare.server.dto;

import org.callistasoftware.netcare.server.model.Patient;

public class PatientDto {

	private Long id;
	
	private String name;
	
	private String civicRegistrationNumber;
	
	private String securityCode;
	
	PatientDto() {}
	
	public static PatientDto create(final String name, final String civicRegistrationNumber, final String securityCode) {
		final PatientDto dto = new PatientDto();
		dto.setCivicRegistrationNumber(civicRegistrationNumber);
		dto.setName(name);
		dto.setId(-1L);
		
		return dto;
	}
	
	public static PatientDto createFromEntity(final Patient patient) {
		return DtoFactory.createDtoFromEntity(patient);
	}

	public Long getId() {
		return id;
	}

	void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	void setName(String name) {
		this.name = name;
	}

	public String getCivicRegistrationNumber() {
		return civicRegistrationNumber;
	}

	void setCivicRegistrationNumber(String civicRegistrationNumber) {
		this.civicRegistrationNumber = civicRegistrationNumber;
	}

	public String getSecurityCode() {
		return securityCode;
	}

	void setSecurityCode(String securityCode) {
		this.securityCode = securityCode;
	}
}
