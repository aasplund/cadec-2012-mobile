package org.callistasoftware.netcare.server.dto;

public class Personnummer {

	private String value;

	public Personnummer(String value) {
		validate(value);
		this.value = value;
	}

	protected void validate(String value) {
		if (value.length() != 12) {
			throw new RuntimeException("Personnummer has length <> 12");
		}
	}

	public String getValue() {
		return this.value;
	}
}
