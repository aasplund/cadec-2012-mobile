package org.callistasoftware.netcare.server.dto;

public enum ProgressStatusTypeDto {
    COMPLETE, USER_SIGN, OUTSTANDING_TRANSACTION;
}
