package org.callistasoftware.netcare.server.exception;

public class SecurityException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public SecurityException(final String message) {
		super(message);
	}

}
