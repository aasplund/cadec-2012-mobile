package org.callistasoftware.netcare.server.model;

public enum AuthenticationType {
	SECURITY_CODE,
	E_ID;
}
