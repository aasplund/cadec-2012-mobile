package org.callistasoftware.netcare.server.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.GrantedAuthorityImpl;
import org.springframework.security.core.userdetails.UserDetails;

@Entity
@Table(name="care_giver")
public class CareGiver implements Serializable, UserDetails {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	
	@Column
	private String hsaId;
	
	@Column
	private String civicRegistrationNumber;
	
	@Column(nullable=false)
	private String name;
	
	CareGiver() {
		
	}
	
	public CareGiver(final String name) {
		this.setName(name);
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getHsaId() {
		return hsaId;
	}

	public void setHsaId(String hsaId) {
		this.hsaId = hsaId;
	}

	public String getCivicRegistrationNumber() {
		return civicRegistrationNumber;
	}

	public void setCivicRegistrationNumber(String civicRegistrationNumber) {
		this.civicRegistrationNumber = civicRegistrationNumber;
	}

	@Override
	public Collection<GrantedAuthority> getAuthorities() {
		final GrantedAuthority r1 = new GrantedAuthorityImpl("ROLE_USER");
		final GrantedAuthority r2 = new GrantedAuthorityImpl("ROLE_CAREGIVER");
		
		final List<GrantedAuthority> roles = new ArrayList<GrantedAuthority>(2);
		roles.add(r1);
		roles.add(r2);
		
		return roles;
	}

	@Override
	public String getPassword() {
		return null;
	}

	@Override
	public String getUsername() {
		return this.hsaId;
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return true;
	}
}
