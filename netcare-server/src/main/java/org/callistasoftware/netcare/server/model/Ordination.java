package org.callistasoftware.netcare.server.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="ordination")
public class Ordination implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	
	@Column(nullable=false)
	private String title;
	
	@Column
	private String description;
	
	@Column
	private OrdinationState state;
	
	@Column
	private Date due;
	
	@Column
	private Date reminder;
	
	@Column
	private String reported;
	
	@ManyToOne(optional=false)
	private Patient patient;
	
	@ManyToMany
	private List<CareGiver> careGivers;
	
	@OneToMany
	private List<SampleValue> sampleValues;

	public Ordination() {
		this.state = OrdinationState.CREATED;
		this.careGivers = new ArrayList<CareGiver>();
		this.sampleValues = new ArrayList<SampleValue>();
	}
	
	public Ordination(final Patient patient, final CareGiver careGiver) {
		this();
		
		this.setPatient(patient);
		this.getCareGivers().add(careGiver);
	}
	
	public Ordination(final Patient patient) {
		this();
		
		this.setPatient(patient);
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public OrdinationState getState() {
		return state;
	}

	public void setState(OrdinationState state) {
		this.state = state;
	}

	public Date getDue() {
		return due;
	}

	public void setDue(Date due) {
		this.due = due;
	}

	public Date getReminder() {
		return reminder;
	}

	public void setReminder(Date reminder) {
		this.reminder = reminder;
	}

	public String getReported() {
		return reported;
	}

	public void setReported(String reported) {
		this.reported = reported;
	}

	public Patient getPatient() {
		return patient;
	}

	public void setPatient(Patient patient) {
		this.patient = patient;
	}

	public List<CareGiver> getCareGivers() {
		return careGivers;
	}

	public void setCareGivers(List<CareGiver> careGivers) {
		this.careGivers = careGivers;
	}

	public List<SampleValue> getSampleValues() {
		return sampleValues;
	}

	public void setSampleValues(List<SampleValue> sampleValues) {
		this.sampleValues = sampleValues;
	}
}
