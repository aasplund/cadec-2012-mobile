package org.callistasoftware.netcare.server.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.GrantedAuthorityImpl;
import org.springframework.security.core.userdetails.UserDetails;

@Entity
@Table(name="patient")
public class Patient implements Serializable, UserDetails {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	
	@Column(length=12, nullable=false, unique=true)
	private String civicRegistrationNumber;
	
	@Column
	private String name;
	
	@Column(length=4, nullable=false)
	private String securityCode;
	
	@Column
	private boolean mobile;
	
	@Column
	private AuthenticationType authenticationType;
	
	@Column
	private String apnRegistrationId;
	
	@Column
	private String c2dmRegistrationId;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCivicRegistrationNumber() {
		return civicRegistrationNumber;
	}

	public void setCivicRegistrationNumber(String civicRegistrationNumber) {
		this.civicRegistrationNumber = civicRegistrationNumber;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSecurityCode() {
		return securityCode;
	}

	public void setSecurityCode(String securityCode) {
		this.securityCode = securityCode;
	}

	public boolean isMobile() {
		return mobile;
	}

	public void setMobile(boolean mobile) {
		this.mobile = mobile;
	}

	public AuthenticationType getAuthenticationType() {
		return authenticationType;
	}

	public void setAuthenticationType(AuthenticationType authenticationType) {
		this.authenticationType = authenticationType;
	}

	@Override
	public Collection<GrantedAuthority> getAuthorities() {
		final GrantedAuthority r1 = new GrantedAuthorityImpl("ROLE_USER");
		final GrantedAuthority r2 = new GrantedAuthorityImpl("ROLE_PATIENT");
		
		final List<GrantedAuthority> roles = new ArrayList<GrantedAuthority>(2);
		roles.add(r1);
		roles.add(r2);
		
		return roles;
	}

	@Override
	public String getPassword() {
		return this.getSecurityCode();
	}

	@Override
	public String getUsername() {
		return this.getCivicRegistrationNumber();
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return true;
	}

	public String getApnRegistrationId() {
		return this.apnRegistrationId;
	}

	public void setApnRegistrationId(String apnRegistrationId) {
		this.apnRegistrationId = apnRegistrationId;
	}

	public String getC2dmRegistrationId() {
		return this.c2dmRegistrationId;
	}

	public void setC2dmRegistrationId(String c2dmRegistrationId) {
		this.c2dmRegistrationId = c2dmRegistrationId;
	}
}
