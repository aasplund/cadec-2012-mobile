package org.callistasoftware.netcare.server.repository;

import org.callistasoftware.netcare.server.model.CareGiver;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CareGiverRepository extends JpaRepository<CareGiver, Long> {

	/**
	 * Find a care giver by HSA id
	 * @param hsaId
	 * @return
	 */
	CareGiver findByHsaId(final String hsaId);
}
