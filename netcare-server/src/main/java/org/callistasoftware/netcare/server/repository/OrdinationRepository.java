package org.callistasoftware.netcare.server.repository;

import java.util.List;

import org.callistasoftware.netcare.server.model.Ordination;
import org.callistasoftware.netcare.server.model.Patient;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OrdinationRepository extends JpaRepository<Ordination, Long> {

	/**
	 * Find ordinations for a given patient
	 * @param patient
	 * @return
	 */
	List<Ordination> findByPatient(final Patient patient);
}
