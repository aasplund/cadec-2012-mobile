package org.callistasoftware.netcare.server.repository;

import org.callistasoftware.netcare.server.model.Patient;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PatientRepository extends JpaRepository<Patient, Long> {

	/**
	 * Find a patient by its civic registration number
	 * @param civicRegistrationNumber
	 * @return
	 */
	Patient findByCivicRegistrationNumber(final String civicRegistrationNumber);
}
