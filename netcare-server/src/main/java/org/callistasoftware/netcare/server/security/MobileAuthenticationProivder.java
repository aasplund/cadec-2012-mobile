package org.callistasoftware.netcare.server.security;

import org.callistasoftware.netcare.server.model.Patient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;

/**
 * Implements an authentication provider. <p>
 * 
 * Using either bank-id or security code depending on user settings.
 * 
 * @author Peter
 */
public class MobileAuthenticationProivder implements AuthenticationProvider {

	private static Logger log = LoggerFactory.getLogger(MobileAuthenticationProivder.class);
	
	@Autowired
	private UserDetailsService userDetailsService;
	
	@Override
	public Authentication authenticate(Authentication authentication) throws AuthenticationException {
		
		log.debug("================ AUTHENTICATION ================");
		
		final Patient p = this.lookup(authentication);
		
		if (!p.isMobile()) {
			throw bad(p);
		}
		
		log.debug("Determine authentication type");
		switch (p.getAuthenticationType()) {
		case SECURITY_CODE:
			this.authPin(p, authentication);
			break;
		case E_ID:
			this.authBankdId(p);
			break;
		default:
			throw this.bad(p);
		}
		
		log.debug("================================================");
		
	    return new UsernamePasswordAuthenticationToken(p, p.getPassword(), p.getAuthorities());
	}


	@Override
	public boolean supports(Class<? extends Object> authentication) {
		return (UsernamePasswordAuthenticationToken.class.isAssignableFrom(authentication));
	}
	
	/**
	 * Authenticates a patient using pin code.
	 * 
	 * @param p the patient
	 * @param a the authentication attempt
	 * @throws {@link BadCredentialsException} on failure
	 */
	protected void authPin(Patient p, Authentication a) {
		log.debug("Authenticating user by pin code");
		String code = p.getSecurityCode();
		
		log.debug("Matching {} with {}", code, a.getCredentials());
		
		if (code == null || !code.equals(a.getCredentials())) {
			throw bad(p);
		}
	}
	
	
	/**
	 * Authenticates a patient using bank-id service.
	 * 
	 * @param p the patient.
	 * @throws {@link uthenticationCredentialsNotFoundException} on failure
	 */
	protected void authBankdId(Patient p) {
		log.debug("Authenticating user by e-id");
	}
	
	/**
	 * Returns a {@link BadCredentialsException}
	 * 
	 * @param p the patient with bad credentials
	 * @return a {@link BadCredentialsException}
	 */
	private BadCredentialsException bad(Patient p) {
		return new BadCredentialsException(p.getId() + ": Mobile channel not activated (user opt-in required)");
	}
	
	/**
	 * Returns a {@link AuthenticationCredentialsNotFoundException}
	 * 
	 * @param name the name of the principal not found
	 * @return a {@link AuthenticationCredentialsNotFoundException}
	 */
	private AuthenticationCredentialsNotFoundException missing(String name) {
		return new AuthenticationCredentialsNotFoundException(name + ": Missing credentials");		
	}

	
	/**
	 * Returns the actual patient associated to an auth. attempt
	 * 
	 * @param a authentication attempt
	 * 
	 * @return the corresponding patient
	 * @throws {@link AuthenticationCredentialsNotFoundException} if no such patient can be found
	 */
	private Patient lookup(Authentication a) {
		String name = a.getName();
		if (name == null || name.length() == 0) {
			throw missing(name);
		}
		
		final UserDetails details = userDetailsService.loadUserByUsername(name);
		if (!Patient.class.isInstance(details)) {
			throw missing(name);
		}
		return (Patient) details;
	}
}
