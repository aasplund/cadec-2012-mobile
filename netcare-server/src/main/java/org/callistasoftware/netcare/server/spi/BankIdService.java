package org.callistasoftware.netcare.server.spi;

import org.callistasoftware.netcare.server.dto.CollectResponseTypeDto;
import org.callistasoftware.netcare.server.dto.Personnummer;
import org.callistasoftware.netcare.server.exception.ServiceException;

public interface BankIdService {

    /**
     * Calls the bank id server for authentication.
     * 
     * @param personnummer
     *            The Personal Number (i.e “personnummer”) of the end user to enroll Format is 12 digits.
     * @return An identifier used later in the Collect method to collect the signature response and to query status.
     */
    public String legitimera(Personnummer personnummer);
    
    /**
     * This method is used to collect a signature after the method Authenticate or Sign has been called successfully.
     * The method also shows the status of the Authenticate or Sign operation.
     * 
     * @param orderRef
     *            The identifier returned by Authenticate or Sign
     * @return Object containing Authentication/Sign status and signature
     */
    public CollectResponseTypeDto collect(String orderRef);

}
