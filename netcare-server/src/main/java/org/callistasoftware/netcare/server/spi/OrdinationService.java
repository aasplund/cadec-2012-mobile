package org.callistasoftware.netcare.server.spi;

import java.util.List;

import org.callistasoftware.netcare.server.dto.OrdinationDto;
import org.callistasoftware.netcare.server.exception.SecurityException;

public interface OrdinationService {

	/**
	 * Load all ordinations for a given patient
	 * @param patientId The id of the patient
	 * @return
	 */
	List<OrdinationDto> loadAllOrdinationsForPatient(final Long patientId) throws SecurityException;
	
	/**
	 * Report an ordination
	 * @param dto - The reported ordination
	 * @return
	 */
	OrdinationDto reportOrdination(final OrdinationDto dto);
	
	/**
	 * Create a new ordination
	 * @param dto
	 * @return
	 */
	OrdinationDto createNewOrdination(final OrdinationDto dto);
}
