package org.callistasoftware.netcare.server.spi;

import org.callistasoftware.netcare.server.dto.PatientDto;

/**
 * Patient service
 * 
 * @author Marcus Krantz [marcus.krantz@callistaenterprise.se]
 *
 */
public interface PatientService {

	/**
	 * Create a new patient
	 * @param dto
	 * @return
	 */
	PatientDto createPatient(final PatientDto dto);
	
	/**
	 * Load a patient by civic registration number
	 * @return
	 */
	PatientDto loadPatientByCivicRegistrationNumber(final String civicRegistrationNumber);
}
