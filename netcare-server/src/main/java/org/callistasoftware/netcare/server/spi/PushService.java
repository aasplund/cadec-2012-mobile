package org.callistasoftware.netcare.server.spi;

import org.callistasoftware.netcare.server.exception.ServiceException;

public interface PushService {

	/**
	 * Sends a push message to the specified account
	 * @param accountId
	 * @param subject
	 * @param message
	 */
	public void push(final Long accountId, final String subject, final String message) throws ServiceException;
}
