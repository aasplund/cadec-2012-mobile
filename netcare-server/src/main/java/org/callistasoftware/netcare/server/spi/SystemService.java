package org.callistasoftware.netcare.server.spi;

public interface SystemService {

	/**
	 * Registers the account for Google Push notifications
	 * @param accountId
	 * @param registrationId
	 */
	void registerAccountForGooglePush(final Long accountId, final String registrationId);
	
	/**
	 * Registers the account for Apple Push notifications
	 * @param accountId
	 * @param registrationId
	 */
	void registerAccountForApplePush(final Long accountId, final String registrationId);
}
