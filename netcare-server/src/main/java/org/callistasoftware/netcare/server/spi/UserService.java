package org.callistasoftware.netcare.server.spi;

import org.springframework.security.core.userdetails.UserDetails;

public interface UserService {

	/**
	 * Load a user
	 * @param id
	 * @return
	 */
	UserDetails loadUserByName(final String username);
}
