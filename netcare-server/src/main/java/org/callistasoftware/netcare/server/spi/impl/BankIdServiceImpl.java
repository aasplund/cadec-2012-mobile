package org.callistasoftware.netcare.server.spi.impl;

import org.callistasoftware.netcare.server.dto.CollectResponseTypeDto;
import org.callistasoftware.netcare.server.dto.Personnummer;
import org.callistasoftware.netcare.server.exception.ServiceException;
import org.callistasoftware.netcare.server.spi.BankIdService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.bankid.rpservice.v1_0_0.RpFault;
import com.bankid.rpservice.v1_0_0.RpServicePortType;
import com.bankid.rpservice.v1_0_0.types.CollectResponseType;

public class BankIdServiceImpl implements BankIdService {
	
	private static Logger log = LoggerFactory.getLogger(BankIdServiceImpl.class);

    private RpServicePortType rpService;

    public BankIdServiceImpl(RpServicePortType rpService) {
        this.rpService = rpService;
    }

    @Override
    public String legitimera(Personnummer personnummer) throws ServiceException {
        if (personnummer == null) {
            throw new IllegalArgumentException("Personummer får inte vara null");
        }

        String orderRef = null;
        try {
        	log.info("Authenticating against bank id server using {}", personnummer.getValue());
            orderRef = rpService.authenticate(personnummer.getValue());
        } catch (RpFault e) {
            throw new ServiceException(e.getMessage(), e);
        }
        
        log.info("Returning orderref {}", orderRef);
        return orderRef;
    }
    
    @Override
    public CollectResponseTypeDto collect(String orderRef) throws ServiceException {
        if(orderRef == null) {
            throw new IllegalArgumentException("OrderRef får inte vara null");
        }
        CollectResponseType response = null;
        try {
            response = rpService.collect(orderRef);
        } catch (RpFault e) {
            throw new ServiceException(e.getMessage(), e);
        }
        return CollectResponseTypeDto.createFromEntity(response);
    }
    
}
