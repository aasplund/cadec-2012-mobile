package org.callistasoftware.netcare.server.spi.impl;

import java.util.ArrayList;
import java.util.List;

import org.callistasoftware.netcare.server.dto.OrdinationDto;
import org.callistasoftware.netcare.server.exception.SecurityException;
import org.callistasoftware.netcare.server.model.Ordination;
import org.callistasoftware.netcare.server.model.Patient;
import org.callistasoftware.netcare.server.repository.OrdinationRepository;
import org.callistasoftware.netcare.server.repository.PatientRepository;
import org.callistasoftware.netcare.server.spi.OrdinationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class OrdinationServiceImpl implements OrdinationService {

	private static Logger log = LoggerFactory.getLogger(OrdinationServiceImpl.class);
	
	@Autowired
	private OrdinationRepository ordinationRepository;
	
	@Autowired
	private PatientRepository patientRepository;
	
	@Override
	public List<OrdinationDto> loadAllOrdinationsForPatient(Long patientId) throws SecurityException {
		
		log.debug("Find ordinations for patient with id {}", patientId);
		
		 final Patient p = this.patientRepository.findOne(patientId);
		 log.debug("Found patient.");
		 
		 final List<Ordination> ordinations = this.ordinationRepository.findByPatient(p);
		 log.debug("Found {} ordinations", ordinations.size());
		 
		 final List<OrdinationDto> dtos = new ArrayList<OrdinationDto>(ordinations.size());
		 
		 for (final Ordination o : ordinations) {
			 log.debug("Creating dto for ordination ({}, {})", o.getTitle(), o.getDescription());
			 dtos.add(OrdinationDto.createFromEntity(o));
		 }
		 
		 return dtos;
	}

	@Override
	public OrdinationDto reportOrdination(OrdinationDto dto) {
		
		/*
		 * Find ordination
		 */
		final Ordination o = this.ordinationRepository.findOne(dto.getId());
		if (o == null) {
			throw new IllegalStateException("Could not find ordination with id " + dto.getId());
		}
		
		o.setReported(Boolean.TRUE.toString());
		return OrdinationDto.createFromEntity(this.ordinationRepository.save(o));
	}

	@Override
	public OrdinationDto createNewOrdination(OrdinationDto dto) {
		log.debug("Creating new ordination with title {}", dto.getTitle());
		
		/*
		 * Get the patient
		 */
		final Patient p = this.patientRepository.findOne(dto.getPatient().getId());
		if (p == null) {
			throw new IllegalStateException("Could not find the patient that would be attached to this ordination");
		}
		
		final Ordination o = new Ordination(p);
		o.setTitle(dto.getTitle());
		o.setDescription(dto.getDescription());
		
		return OrdinationDto.createFromEntity(this.ordinationRepository.save(o));
	}

}
