package org.callistasoftware.netcare.server.spi.impl;

import org.callistasoftware.netcare.server.dto.PatientDto;
import org.callistasoftware.netcare.server.exception.ServiceException;
import org.callistasoftware.netcare.server.model.AuthenticationType;
import org.callistasoftware.netcare.server.model.Ordination;
import org.callistasoftware.netcare.server.model.Patient;
import org.callistasoftware.netcare.server.repository.OrdinationRepository;
import org.callistasoftware.netcare.server.repository.PatientRepository;
import org.callistasoftware.netcare.server.spi.PatientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class PatientServiceImpl implements PatientService {

	@Autowired
	private OrdinationRepository oRepo;
	
	@Autowired
	private PatientRepository patientRepository;
	
	@Override
	public PatientDto createPatient(PatientDto dto) {
		/*
		 * Check if patient with crn already exist
		 */
		final Patient p = this.patientRepository.findByCivicRegistrationNumber(dto.getCivicRegistrationNumber());
		if (p != null) {
			throw new IllegalStateException("Det finns redan en användare med samma användarnamn. Testa med ett annat.");
		}
		
		final Patient newPatient = new Patient();
		newPatient.setSecurityCode(dto.getSecurityCode());
		newPatient.setCivicRegistrationNumber(dto.getCivicRegistrationNumber());
		newPatient.setName(dto.getName());
		newPatient.setMobile(true);
		newPatient.setAuthenticationType(AuthenticationType.SECURITY_CODE);
		
		final Patient save = this.patientRepository.save(newPatient);
		
		final Ordination o = new Ordination(save);
		o.setTitle("Motion");
		o.setDescription("Promenera en kilometer");
		
		this.oRepo.save(o);
		
		final Ordination o2 = new Ordination(save);
		o2.setTitle("Medicin");
		o2.setDescription("Ta en ipren");
		
		this.oRepo.save(o2);
		
		return PatientDto.createFromEntity(save);
	}

	@Override
	public PatientDto loadPatientByCivicRegistrationNumber(
			String civicRegistrationNumber) {
		final Patient p = this.patientRepository.findByCivicRegistrationNumber(civicRegistrationNumber);
		if(p == null) {
			throw new ServiceException("Patient {} could not be found.");
		}
		
		return PatientDto.createFromEntity(p);
	}

}
