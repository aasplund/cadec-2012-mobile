package org.callistasoftware.netcare.server.spi.impl;

import org.callistasoftware.netcare.server.exception.ServiceException;
import org.callistasoftware.netcare.server.model.Patient;
import org.callistasoftware.netcare.server.repository.PatientRepository;
import org.callistasoftware.netcare.server.spi.PushService;
import org.callistasoftware.netcare.server.spi.helper.C2DMMessager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PushServiceImpl implements PushService {

	private static final Logger log = LoggerFactory.getLogger(PushServiceImpl.class);
	
	@Autowired
	private C2DMMessager googleMessaging;
	
	@Autowired
	private PatientRepository patientRepository;
	
	@Override
	public void push(Long accountId, String subject, String message)
			throws ServiceException {
		
		log.debug("Push notification to client");
		
		final Patient p = this.patientRepository.findOne(accountId);
		if (p == null) {
			throw new ServiceException("Could not find any patient with id: " + accountId);
		}
		
		log.debug("Patient is mobile user: {}", p.isMobile());
		log.debug("Patient c2dm: {}", p.getC2dmRegistrationId());
		log.debug("Patient apn: {}", p.getApnRegistrationId());
		
		if (!p.isMobile()) {
			throw new ServiceException("Patient is not registered for mobile usage");
		}
		
		if (p.getC2dmRegistrationId() == null && p.getApnRegistrationId() == null) {
			throw new ServiceException("Patient is not registered for push notifications");
		}
		
		if (p.getC2dmRegistrationId() != null) {
			/*
			 * Push through google
			 */
			final String auth = this.googleMessaging.fetchGoogleAuthToken();
			this.googleMessaging.sendGooglePushNotification(auth, p.getC2dmRegistrationId(), subject, message, "", "");
		}
		
		if (p.getApnRegistrationId() != null) {
			/*
			 * Push through apple
			 */
		}
	}

}
