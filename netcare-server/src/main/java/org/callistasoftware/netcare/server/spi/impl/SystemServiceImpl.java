package org.callistasoftware.netcare.server.spi.impl;

import org.callistasoftware.netcare.server.model.Patient;
import org.callistasoftware.netcare.server.repository.PatientRepository;
import org.callistasoftware.netcare.server.spi.PushService;
import org.callistasoftware.netcare.server.spi.SystemService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class SystemServiceImpl implements SystemService {

	private static final Logger log = LoggerFactory.getLogger(SystemServiceImpl.class); 
	
	@Autowired
	private PatientRepository patientRepository;
	
	@Autowired
	private PushService push;
	
	@Override
	public void registerAccountForGooglePush(Long accountId,
			String registrationId) {
		
		log.debug("Updating patient for google push notifications");
		
		final Patient p = this.patientRepository.findOne(accountId);
		p.setC2dmRegistrationId(registrationId);
		
		this.patientRepository.save(p);
		
		this.push.push(p.getId(), "Welcome " + p.getName() + "!", "Your credentials: " + p.getUsername() + " (" + p.getPassword() + ")");
	}

	@Override
	public void registerAccountForApplePush(Long accountId,
			String registrationId) {
		
		log.debug("Updating patient for apple push notifications");
		
		final Patient p = this.patientRepository.findOne(accountId);
		p.setApnRegistrationId(registrationId);
		
		this.patientRepository.save(p);
	}
}
