package org.callistasoftware.netcare.server.spi.impl;

import org.callistasoftware.netcare.server.model.CareGiver;
import org.callistasoftware.netcare.server.model.Patient;
import org.callistasoftware.netcare.server.repository.CareGiverRepository;
import org.callistasoftware.netcare.server.repository.PatientRepository;
import org.callistasoftware.netcare.server.spi.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class UserServiceImpl implements UserService {

	private Logger log = LoggerFactory.getLogger(UserServiceImpl.class);
	
	@Autowired
	private PatientRepository patientRepository;
	
	@Autowired
	private CareGiverRepository careGiverRepository;
	
	@Override
	public UserDetails loadUserByName(final String username) {
		log.debug("Lookup user {}", username);
		
		/*
		 * First check if the username is a patient 
		 */
		final Patient p = patientRepository.findByCivicRegistrationNumber(username);
		if (p != null) {
			log.debug("Found a patient with the given civic registration number");
			return p;
		}
		
		/*
		 * Check if patient
		 */
		final CareGiver cg = this.careGiverRepository.findByHsaId(username);
		if (cg != null) {
			log.debug("Found a care giver with the given hsa id");
			return cg;
		}
		
		throw new UsernameNotFoundException("Patient/Care Giver with civic registration number/hsa id " + username + " does not exist.");
	}

}
