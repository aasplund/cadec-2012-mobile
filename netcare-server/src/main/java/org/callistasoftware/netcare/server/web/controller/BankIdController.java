package org.callistasoftware.netcare.server.web.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.callistasoftware.netcare.server.dto.CollectResponseTypeDto;
import org.callistasoftware.netcare.server.dto.OrdinationDto;
import org.callistasoftware.netcare.server.dto.Personnummer;
import org.callistasoftware.netcare.server.dto.ProgressStatusTypeDto;
import org.callistasoftware.netcare.server.exception.SecurityException;
import org.callistasoftware.netcare.server.exception.ServiceException;
import org.callistasoftware.netcare.server.spi.BankIdService;
import org.callistasoftware.netcare.server.spi.OrdinationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(value = "/bankid")
public class BankIdController {

    private BankIdService bankIdService;
    @Autowired
    private OrdinationService ordinationService;

    @Autowired
    public BankIdController(BankIdService service) {
        bankIdService = service;
    }

    @RequestMapping(value = "/legitimera/{personnummer}", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public String authenticate(@PathVariable String personnummer, HttpServletResponse response) throws IOException {
        try {
            return bankIdService.legitimera(new Personnummer(personnummer));
        } catch (Exception e) {
            response.sendError(HttpServletResponse.SC_NOT_FOUND, e.getMessage());
        }
        return "";
    }

    @RequestMapping(value = "/collect/{orderRef}", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public Boolean collect(@PathVariable String orderRef, HttpServletResponse response)
            throws IOException {
        try {
            CollectResponseTypeDto collectResponse = bankIdService.collect(orderRef);
            return collectResponse.getProgressStatus().equals(ProgressStatusTypeDto.COMPLETE);
        } catch (ServiceException e) {
            response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, e.getMessage());
            return Boolean.FALSE;
        }
    }

    @RequestMapping(value = "ordinations/list", method = RequestMethod.GET, produces = "application/json; charset=UTF-8")
    @ResponseBody
    public OrdinationDto[] getOrdinationsForPatient(
            @RequestParam(value = "patientId", required = true) final Long patientId) throws SecurityException {
        long dummyPatientId = 1234567890004L;
        final List<OrdinationDto> dtos = this.ordinationService.loadAllOrdinationsForPatient(dummyPatientId);
        return dtos.toArray(new OrdinationDto[dtos.size()]);
    }
}
