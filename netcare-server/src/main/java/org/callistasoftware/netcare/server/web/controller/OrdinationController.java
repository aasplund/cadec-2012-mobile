package org.callistasoftware.netcare.server.web.controller;

import java.util.List;

import org.callistasoftware.netcare.server.dto.OrdinationDto;
import org.callistasoftware.netcare.server.exception.SecurityException;
import org.callistasoftware.netcare.server.spi.OrdinationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@Controller
@RequestMapping(value="/ordinations")
public class OrdinationController {

	private static Logger log = LoggerFactory.getLogger(OrdinationController.class);
	
	@Autowired
	private OrdinationService ordinationService;
	
	@RequestMapping(value="/basic/list", method=RequestMethod.GET, produces="application/json")
	@ResponseBody
	public OrdinationDto[] loadOrdinationsBasic(@RequestParam(value="patientId", required=true) final Long patientId) throws SecurityException {
		
		log.info("List ordinations for patient {} using http", patientId);
		
		final List<OrdinationDto> dtos = this.ordinationService.loadAllOrdinationsForPatient(patientId);
		return dtos.toArray(new OrdinationDto[dtos.size()]);
	}
	
	@RequestMapping(value="/secure/list", method=RequestMethod.GET, produces="application/json")
	@ResponseBody
	public OrdinationDto[] loadOrdinationsSecure(@RequestParam(value="patientId", required=true) final Long patientId) throws SecurityException {
		
		log.info("List ordinations for patient {} using https", patientId);
		
		final List<OrdinationDto> dtos = this.ordinationService.loadAllOrdinationsForPatient(patientId);
		return dtos.toArray(new OrdinationDto[dtos.size()]);
	}
	
	@RequestMapping(value="/list", method=RequestMethod.GET, produces="application/json; charset=UTF-8")
	@ResponseBody
	public OrdinationDto[] getOrdinationsForPatient(
			@RequestParam(value="patientId", required=true) final Long patientId) throws SecurityException {
		log.info("List ordinations for patient {}", patientId);
		final List<OrdinationDto> dtos = this.ordinationService.loadAllOrdinationsForPatient(patientId);
		return dtos.toArray(new OrdinationDto[dtos.size()]);
	}
	
	@RequestMapping(value="/report", method=RequestMethod.POST, produces="application/json")
	@ResponseBody
	public OrdinationDto reportOrdination(@RequestBody final OrdinationDto ordination) {
		log.info("Reporting ordination {}", ordination.getId());
		return this.ordinationService.reportOrdination(ordination);
	}
	
	@RequestMapping(value="/create", method=RequestMethod.POST, produces="application/json")
	@ResponseBody
	public OrdinationDto createNewOrdination(@RequestBody final OrdinationDto ordination) {
		return this.ordinationService.createNewOrdination(ordination);
	}
	
	@ExceptionHandler(MissingServletRequestParameterException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ResponseBody
	public String handleException(final MissingServletRequestParameterException e) {
		return e.getMessage();
	}
	
	@ExceptionHandler(SecurityException.class)
	@ResponseStatus(HttpStatus.FORBIDDEN)
	@ResponseBody
	public String handleException(final SecurityException e) {
		return e.getMessage();
	}
	
	@ExceptionHandler(Exception.class)
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	@ResponseBody
	public String handleException(final Exception e) {
		return e.getMessage();
	}
}
