package org.callistasoftware.netcare.server.web.controller;

import java.security.Principal;

import org.callistasoftware.netcare.server.dto.PatientDto;
import org.callistasoftware.netcare.server.spi.PatientService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(value="/patients")
public class PatientController {

	private static final Logger log = LoggerFactory.getLogger(PatientController.class);
	
	@Autowired
	private PatientService patientService;
	
	@RequestMapping(value="/secure/login", method=RequestMethod.GET, produces="application/json")
	@ResponseBody
	public PatientDto secureLogin(final Principal principal) {
		log.info("Fetch principal {}", principal.getName());
		return this.patientService.loadPatientByCivicRegistrationNumber(principal.getName());
	}
	
	@RequestMapping(value="/basic/login", method=RequestMethod.GET, produces="application/json")
	@ResponseBody
	public PatientDto plainLogin(final Principal principal) {
		log.info("Plain login with basic authentication. Principal: {}", principal.getName());
		return this.patientService.loadPatientByCivicRegistrationNumber(principal.getName());
	}
	
	@RequestMapping(value="/create", method=RequestMethod.POST, produces="application/json")
	@ResponseBody
	public PatientDto createPatient(@RequestBody final PatientDto dto) {
		return this.patientService.createPatient(dto);
	}
}
