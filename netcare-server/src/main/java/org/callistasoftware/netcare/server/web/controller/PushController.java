package org.callistasoftware.netcare.server.web.controller;

import org.callistasoftware.netcare.server.exception.ServiceException;
import org.callistasoftware.netcare.server.spi.PushService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class PushController {

	private static final Logger log = LoggerFactory.getLogger(PushController.class);
	
	@Autowired
	private PushService push;
	
	@RequestMapping(value="/push", method=RequestMethod.GET, produces="application/json")
	@ResponseBody
	public String push(
			@RequestParam(value="account", required=true) final Long accountId
			, @RequestParam(value="subject", required=true) final String subject
			, @RequestParam(value="message", required=true) final String message) {
		
		log.info("Pushing notification to account {}", accountId);
		
		try {
			this.push.push(accountId, subject, message);
			return "Message was successfully pushed";
		} catch (final ServiceException e) {
			log.error("Error caught: {}", e.getMessage());
			return e.getMessage();
		}
	}
}
