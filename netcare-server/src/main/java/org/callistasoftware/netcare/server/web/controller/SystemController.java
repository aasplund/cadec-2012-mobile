package org.callistasoftware.netcare.server.web.controller;

import org.callistasoftware.netcare.server.dto.C2DMRegistrationDto;
import org.callistasoftware.netcare.server.spi.SystemService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/system")
public class SystemController {

	private static Logger log = LoggerFactory.getLogger(SystemController.class);
	
	@Autowired
	private SystemService systemService;
	
	@RequestMapping(value="/c2dm", method=RequestMethod.PUT)
	@ResponseBody
	public void registerForGooglePush(@RequestBody final C2DMRegistrationDto dto) {
		log.info("Registering account for Google push notifications.");
		log.debug("Account: {}", dto.getAccountId());
		log.debug("Registration id: {}", dto.getRegistrationId());
		
		this.systemService.registerAccountForGooglePush(dto.getAccountId(), dto.getRegistrationId());
	}
	
	@RequestMapping(value="/apn", method=RequestMethod.PUT)
	@ResponseBody
	public void registerForApplePush(@RequestParam(value="account", required=true) final Long accountId
			, @RequestParam(value="registrationId", required=true) final String registrationId
			, @RequestBody final Object obj) {
		log.info("Registering account {} for Apple push notifications.");
		log.debug("Account: {}", accountId);
		log.debug("Registration id: {}", registrationId);
		
		this.systemService.registerAccountForApplePush(accountId, registrationId);
	}
}
