package org.callistasoftware.netcare.server.web.controller;

import org.callistasoftware.netcare.server.dto.PatientDto;
import org.callistasoftware.netcare.server.spi.PatientService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/user")
public class UserController {

	private static Logger log = LoggerFactory.getLogger(UserController.class);
	
	@Autowired
	private PatientService service;
	
	@RequestMapping(value="/new")
	@ResponseBody
	public PatientDto createNewUser(@RequestBody final PatientDto patient) {
		
		log.info("Creating user " + patient.getCivicRegistrationNumber());
		final PatientDto newPatient = service.createPatient(patient);
		return newPatient;
	}
}
