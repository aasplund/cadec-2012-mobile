package org.callistasoftware.netcare.server.web.listener;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletContextEvent;

import org.callistasoftware.netcare.server.model.AuthenticationType;
import org.callistasoftware.netcare.server.model.CareGiver;
import org.callistasoftware.netcare.server.model.Ordination;
import org.callistasoftware.netcare.server.model.Patient;
import org.callistasoftware.netcare.server.repository.CareGiverRepository;
import org.callistasoftware.netcare.server.repository.OrdinationRepository;
import org.callistasoftware.netcare.server.repository.PatientRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

public class ApplicationListener extends ContextLoaderListener {

	private static Logger log = LoggerFactory.getLogger(ApplicationListener.class);

	@Override
	public void contextDestroyed(ServletContextEvent event) {
		super.contextDestroyed(event);
		log.info("================ NETCARE SHUTDOWN ================");
	}

	@Override
	public void contextInitialized(ServletContextEvent event) {
		super.contextInitialized(event);
		
		log.info("Populating netcare database...");
		
		final WebApplicationContext wctx = WebApplicationContextUtils.getWebApplicationContext(event.getServletContext());
		final PatientRepository pr = wctx.getBean(PatientRepository.class);
		final CareGiverRepository cgr = wctx.getBean(CareGiverRepository.class);
		
		final OrdinationRepository or = wctx.getBean(OrdinationRepository.class);
		
		final List<CareGiver> cgs = new ArrayList<CareGiver>();
		
		final CareGiver cg1 = new CareGiver("Marcus Krantz");
		cgs.add(cg1);
		
		final CareGiver cg2 = new CareGiver("Peter Larsson");
		cgs.add(cg2);
		
		final CareGiver cg3 = new CareGiver("Hans Thunberg");
		cgs.add(cg3);
		
		cgr.save(cgs);
		
		final List<Patient> patients = new ArrayList<Patient>();
		
		final Patient p1 = new Patient();
		p1.setName("Johannes Carlen");
		p1.setSecurityCode("0000");
		p1.setCivicRegistrationNumber("123456789001");
		p1.setMobile(true);
		p1.setAuthenticationType(AuthenticationType.E_ID);
		patients.add(p1);
		
		final Patient p2 = new Patient();
		p2.setName("Kristin Luhr");
		p2.setSecurityCode("0000");
		p2.setCivicRegistrationNumber("123456789002");
		p2.setMobile(true);
		p2.setAuthenticationType(AuthenticationType.E_ID);
		patients.add(p2);
		
		final Patient p3 = new Patient();
		p3.setName("Sedina Oruc");
		p3.setSecurityCode("0000");
		p3.setCivicRegistrationNumber("123456789003");
		p3.setMobile(true);
		p3.setAuthenticationType(AuthenticationType.E_ID);
		patients.add(p3);
		
		final Patient p4 = new Patient();
		p4.setName("Christian Hilmersson");
		p4.setSecurityCode("0000");
		p4.setCivicRegistrationNumber("123456789004");
		p4.setMobile(true);
		p4.setAuthenticationType(AuthenticationType.SECURITY_CODE);
		patients.add(p4);
		
		pr.save(patients);
		
		final Calendar c = Calendar.getInstance();
		final Date now = c.getTime();
		
		final Date dueDate = new Date(now.getTime() + 1000 * 3600);
		final Date alarm = new Date(now.getTime() + 1000 * 300);
		
		final List<Ordination> ords = new ArrayList<Ordination>();
		
		final Ordination o1 = new Ordination(p4, cg1);
		o1.setTitle("Motion");
		o1.setDescription("Du måste motionera.");
		o1.setDue(dueDate);
		o1.setReminder(alarm);
		ords.add(o1);
		
		final Ordination o2 = new Ordination(p4, cg2);
		o2.setTitle("Sömn");
		o2.setDescription("Du måste sova.");
		o2.setDue(dueDate);
		o2.setReminder(alarm);
		ords.add(o2);
		
		or.save(ords);
		
		log.info("================ NETCARE STARTED ================");
	}
}
