use netcare;

insert into care_giver (name) values ('Marcus');
insert into care_giver (name) values ('Peter');
insert into care_giver (name) values ('Hans');
insert into care_giver (name) values ('Johannes');
insert into care_giver (name) values ('Kristin');
insert into care_giver (name) values ('Christian');
insert into care_giver (name) values ('Sedina');

insert into patient (name, civicRegistrationNumber) values ('Arne', '1234567890001');
insert into patient (name, civicRegistrationNumber) values ('Barne', '1234567890002');
insert into patient (name, civicRegistrationNumber) values ('Carne', '1234567890003');
insert into patient (name, civicRegistrationNumber) values ('Darne', '1234567890004');

insert into ordination (title, description, due, reminder, )