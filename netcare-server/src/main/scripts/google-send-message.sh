#!/bin/bash
#
# example: send message to android app using an existing google account
#
email=krantz.marcus@gmail.com
pkg=org.callistasoftware.netcare.android
collapse_key="act"

[ $# -ne 2 ] && { echo "Usage: $0 <app.rid> <message> "; exit 1; }

app_rid=$1
message=$2


# get password
read -s -p "Password: " pwd
echo ""

# request token
rsp=`curl --silent https://www.google.com/accounts/ClientLogin -d Email=$email -d Passwd=$pwd -d accountType=HOSTED_OR_GOOGLE -d source=$pkg -d service=ac2dm`
auth_token=`expr "$rsp" : '.*Auth=\(.*\)'`
[ ${#auth_token} -eq 0 ] && { echo $rsp; exit 1; }

echo "Token $auth_token"

# send message
curl --header "Authorization: GoogleLogin auth=$auth_token" "https://android.apis.google.com/c2dm/send" -d "registration_id=$app_rid" -d "collapse_key=$collapse_key" -d "data.message=$message" -d "data.title=NetCare" -d "data.timestamp=0" -k
