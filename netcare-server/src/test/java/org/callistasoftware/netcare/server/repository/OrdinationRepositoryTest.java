package org.callistasoftware.netcare.server.repository;

import java.util.List;

import junit.framework.TestCase;

import org.callistasoftware.netcare.server.model.CareGiver;
import org.callistasoftware.netcare.server.model.Ordination;
import org.callistasoftware.netcare.server.model.Patient;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations="classpath:/netcare-test-config.xml")
public class OrdinationRepositoryTest extends TestCase {

	@Autowired
	private OrdinationRepository ordinationRepository;
	
	@Autowired
	private PatientRepository patientRepository;
	
	@Autowired
	private CareGiverRepository careGiverRepository;
	
	@Transactional
	@Rollback(value=true)
	@Test
	public void insertAndFind() {
		
		final Patient p = new Patient();
		p.setSecurityCode("0000");
		p.setCivicRegistrationNumber("123456789000");
		p.setName("Marcus");
		
		this.patientRepository.save(p);
		
		final CareGiver cg = new CareGiver("Test");
		this.careGiverRepository.save(cg);
		
		final Ordination o = new Ordination(p, cg);
		o.setTitle("Motion");
		
		this.ordinationRepository.save(o);
		
		long count = this.ordinationRepository.count();
		assertEquals(1, count);
		
		final List<Ordination> ords = this.ordinationRepository.findAll();
		assertNotNull(ords);
		assertEquals(1, ords.size());
		assertEquals(o.getTitle(), ords.get(0).getTitle());
		assertNotNull(ords.get(0).getPatient());
		assertNotNull(ords.get(0).getCareGivers());
	}
	
}
