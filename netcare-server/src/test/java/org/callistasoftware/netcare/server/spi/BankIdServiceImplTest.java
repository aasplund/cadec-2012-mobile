package org.callistasoftware.netcare.server.spi;

import org.callistasoftware.netcare.server.dto.Personnummer;
import org.callistasoftware.netcare.server.spi.impl.BankIdServiceImpl;
import org.junit.Before;
import org.junit.Test;

import com.bankid.rpservice.v1_0_0.RpFault;
import com.bankid.rpservice.v1_0_0.RpServicePortType;
import com.bankid.rpservice.v1_0_0.types.CollectResponseType;
import com.bankid.rpservice.v1_0_0.types.SignRequestType;


public class BankIdServiceImplTest {
	
	private BankIdService bankIdService;
	
	@Before
	public void setup() {
		RpServicePortType rpService = new MockRpServicePortType();
		bankIdService = new BankIdServiceImpl(rpService );
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void shouldThrowIllegalArgumentExceptionIfPersonnummerIsNull() {
		bankIdService.legitimera(null);
	}

	@Test(expected=RuntimeException.class)
	public void shouldThrowRuntimeExceptionIfFaultIsReturned() {
		Personnummer personnummer = new Personnummer("1927051768453");
		bankIdService.legitimera(personnummer);
	}
	
	public class MockRpServicePortType implements RpServicePortType {
		
		@Override
		public String sign(SignRequestType signRequest) throws RpFault {
			// TODO Auto-generated method stub
			return null;
		}
		
		@Override
		public CollectResponseType collect(String orderRef) throws RpFault {
			// TODO Auto-generated method stub
			return null;
		}
		
		@Override
		public String authenticate(String personalNumber) throws RpFault {
			throw new RpFault();
		}
	}
	
}
