package org.callistasoftware.netcare.server.spi;

import java.util.ArrayList;
import java.util.List;

import junit.framework.TestCase;

import org.callistasoftware.netcare.server.dto.OrdinationDto;
import org.callistasoftware.netcare.server.model.Ordination;
import org.callistasoftware.netcare.server.model.Patient;
import org.callistasoftware.netcare.server.repository.OrdinationRepository;
import org.callistasoftware.netcare.server.repository.PatientRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations="classpath:/netcare-test-config.xml")
public class OrdinationServiceTest extends TestCase {

	@Autowired
	private PatientRepository patientRepository;
	
	@Autowired
	private OrdinationRepository ordinationRepository;
	
	@Autowired
	private OrdinationService service;
	
	@Transactional
	@Test
	public void testFindByPatient() throws Exception {
		
		Patient p = new Patient();
		p.setSecurityCode("0000");
		p.setName("Marcus");
		p.setCivicRegistrationNumber("1234567890000");
		
		p = this.patientRepository.save(p);
		
		final List<Ordination> ords = new ArrayList<Ordination>();
		for (int i = 0; i < 10; i++) {
			final Ordination o = new Ordination(p);
			o.setTitle("Ordination #" + i);
			
			ords.add(o);
		}
		
		this.ordinationRepository.save(ords);
		
		final List<OrdinationDto> patientOrdinations = this.service.loadAllOrdinationsForPatient(p.getId());
		assertNotNull(patientOrdinations);
		assertEquals(10, patientOrdinations.size());
	}
}
