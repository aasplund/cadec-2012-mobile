package org.callista.cadec.android;

import junit.framework.Test;
import junit.framework.TestSuite;
import android.test.suitebuilder.TestSuiteBuilder;


/**
 * A test suite containing all tests for the demo app.
 * 
 * To run all suites found in this apk: $ adb shell am instrument -w \
 * org.callista.cadec.android.tests/android.test.InstrumentationTestRunner
 * 
 * To run just this suite from the command line: $ adb shell am instrument -w \
 * -e class org.callista.cadec.android.AllTests \
 * org.callista.cadec.android.tests/android.test.InstrumentationTestRunner
 * 
 */
public class AllTests extends TestSuite {

	public static Test suite() {
		return new TestSuiteBuilder(AllTests.class)
				.includeAllPackagesUnderHere().build();
	}
}
