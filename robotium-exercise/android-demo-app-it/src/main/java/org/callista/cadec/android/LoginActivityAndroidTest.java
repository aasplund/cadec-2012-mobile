package org.callista.cadec.android;

import android.content.Context;
import android.test.AndroidTestCase;
import android.test.suitebuilder.annotation.SmallTest;
import android.view.FocusFinder;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

/**
 * 
 * This exercises the same logic as {@link LoginActivityInstrumentationTest} but
 * in a lighter weight manner; it doesn't need to launch the activity.
 * 
 * {@link LoginActivityInstrumentationTest} is still useful to verify that, at
 * an end to end level.
 * 
 * A good complementary way to use both types of tests might be to have more
 * exhaustive coverage in the lighter weight test case, and a few end to end
 * scenarios in the functional
 * {@link android.test.ActivityInstrumentationTestCase2}. This would provide
 * reasonable assurance that the end to end system is working, while avoiding
 * the overhead of having every corner case exercised in the slower, heavier
 * weight way.
 * 
 * Even as a lighter weight test, this test still needs access to a
 * {@link Context} to inflate the file, which is why it extends
 * {@link AndroidTestCase}.
 * 
 * If you ever need a context to do your work in tests, you can extend
 * {@link AndroidTestCase}, and when run via an
 * {@link android.test.InstrumentationTestRunner}, the context will be injected
 * for you.
 * 
 * See {@link org.callista.cadec.android.AllTests} for documentation on running
 * all tests and individual tests in this application.
 */
public class LoginActivityAndroidTest extends AndroidTestCase {

	private Button mButton;
	private EditText ffUsername;
	private EditText ffPassword;
	private TextView lblResult;
	private ViewGroup mLayout;

	@Override
	protected void setUp() throws Exception {
		super.setUp();

		final Context context = getContext();
		final LayoutInflater inflater = LayoutInflater.from(context);
		mLayout = (ViewGroup) inflater.inflate(R.layout.main, null);

		mButton = (Button) mLayout.findViewById(R.id.login_button);
		ffUsername = (EditText) mLayout.findViewById(R.id.username);
		ffPassword = (EditText) mLayout.findViewById(R.id.password);
		lblResult = (TextView) mLayout.findViewById(R.id.result);
	}

	/**
	 * The name 'test preconditions' is a convention to signal that if this test
	 * doesn't pass, the test case was not set up properly and it might explain
	 * any and all failures in other tests. This is not guaranteed to run before
	 * other tests, as junit uses reflection to find the tests.
	 */
	@SmallTest
	public void testPreconditions() {
		assertNotNull(mLayout);

		assertNotNull(lblResult);
		assertNotNull(ffPassword);
		assertNotNull(ffUsername);
		assertNotNull(mButton);

	}

}
