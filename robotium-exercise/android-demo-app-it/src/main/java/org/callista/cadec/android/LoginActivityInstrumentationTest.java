package org.callista.cadec.android;

import android.test.ActivityInstrumentationTestCase2;
import android.test.suitebuilder.annotation.MediumTest;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

/**
 * An example of an {@link ActivityInstrumentationTestCase2} of a specific
 * activity {@link LoginActivity}.
 * 
 * By extending {@link ActivityInstrumentationTestCase2}, the target activity is
 * automatically launched and finished before and after each test.
 * 
 * This also extends {@link android.test.InstrumentationTestCase}, which
 * provides access to methods for sending events to the target activity, such as
 * key and touch events. See {@link #sendKeys}.
 * 
 * In general, {@link android.test.InstrumentationTestCase}s and
 * {@link ActivityInstrumentationTestCase2}s are heavier weight functional tests
 * available for end to end testing of your user interface.
 * 
 * When run via a {@link android.test.InstrumentationTestRunner}, the necessary
 * {@link android.app.Instrumentation} will be injected for you to user via
 * {@link #getInstrumentation} in your tests.
 * 
 * See {@link org.callista.cadec.android.AllTests} for documentation on running
 * all tests and individual tests in this application.
 */
public class LoginActivityInstrumentationTest extends ActivityInstrumentationTestCase2<LoginActivity> {

	private Button mButton;
	private EditText ffUsername;
	private EditText ffPassword;
	private TextView lblResult;
	private LoginActivity activity;

	/*
	 * Constructor for the test class. Required by Android test classes. The
	 * constructor must call the super constructor, providing the Android
	 * package name of the app under test and the Java class name of the
	 * activity in that application that handles the MAIN intent.
	 */
	public LoginActivityInstrumentationTest(Class<LoginActivity> activityClass) {
		super(activityClass);
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();

		/*
		 * Start the app under test by starting its main activity. The test
		 * runner already knows which activity this is from the call to the
		 * super constructor. The tests can now use instrumentation to directly
		 * access the main activity through activity.
		 */
		activity = getActivity();
	}

	/**
	 * The name 'test preconditions' is a convention to signal that if this test
	 * doesn't pass, the test case was not set up properly and it might explain
	 * any and all failures in other tests. This is not guaranteed to run before
	 * other tests, as junit uses reflection to find the tests.
	 */
	@MediumTest
	public void testPreconditions() {
		assertNotNull(activity);

		mButton = (Button) activity.findViewById(R.id.login_button);
		ffUsername = (EditText) activity.findViewById(R.id.username);
		ffPassword = (EditText) activity.findViewById(R.id.password);
		lblResult = (TextView) activity.findViewById(R.id.result);

		assertNotNull(lblResult);
		assertNotNull(ffPassword);
		assertNotNull(ffUsername);
		assertNotNull(mButton);
	}

}
