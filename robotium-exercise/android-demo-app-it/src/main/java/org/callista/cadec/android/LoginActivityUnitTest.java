package org.callista.cadec.android;

import android.content.Intent;
import android.test.ActivityUnitTestCase;
import android.test.suitebuilder.annotation.SmallTest;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

/**
 * 
 * ActivityUnitTestCase provides isolated testing of a single activity. The
 * activity under test will be created with minimal connection to the system
 * infrastructure, and you can inject mocked or wrappered versions of many of
 * Activity's dependencies. Most of the work is handled automatically here by
 * setUp() and tearDown().
 * 
 * See {@link org.callista.cadec.android.AllTests} for documentation on running
 * all tests and individual tests in this application.
 */
public class LoginActivityUnitTest extends ActivityUnitTestCase<LoginActivity> {

	private Intent mStartIntent;

	public LoginActivityUnitTest() {
		super(LoginActivity.class);
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		mStartIntent = new Intent(Intent.ACTION_MAIN);
	}

	/**
	 * The name 'test preconditions' is a convention to signal that if this test
	 * doesn't pass, the test case was not set up properly and it might explain
	 * any and all failures in other tests. This is not guaranteed to run before
	 * other tests, as junit uses reflection to find the tests.
	 */
	@SmallTest
	public void testPreconditions() {
		startActivity(mStartIntent, null, null);
		Button mButton = (Button) getActivity().findViewById(R.id.login_button);
		EditText ffUsername = (EditText) getActivity().findViewById(R.id.username);
		EditText ffPassword = (EditText) getActivity().findViewById(R.id.password);

		assertNotNull(getActivity());
		assertNotNull(ffPassword);
		assertNotNull(ffUsername);
		assertNotNull(mButton);
	}

	@SmallTest
	public void testLoginFailureGivesErrorText() {
		startActivity(mStartIntent, null, null);

		EditText ffUsername = (EditText) getActivity().findViewById(R.id.username);
		ffUsername.setText("UnknownUserName");

		Button mButton = (Button) getActivity().findViewById(R.id.login_button);
		mButton.performClick();

		TextView lblResult = (TextView) getActivity().findViewById(R.id.result);

		assertEquals("Felaktigt användarnamn/lösenord", lblResult.getText().toString());
		assertFalse(isFinishCalled());
	}

	@SmallTest
	public void testLoginSuccessTriggersNextActivityAndFinishesItself() {
		startActivity(mStartIntent, null, null);

		EditText ffUsername = (EditText) getActivity().findViewById(R.id.username);
		ffUsername.setText("me");

		EditText ffPassword = (EditText) getActivity().findViewById(R.id.password);
		ffPassword.setText("me");

		Button mButton = (Button) getActivity().findViewById(R.id.login_button);
		mButton.performClick();

		assertNextIntentIsSpinnerActivity();
		assertTrue(isFinishCalled());
	}

	private void assertNextIntentIsSpinnerActivity() {
		String nextIntent = getStartedActivityIntent().getComponent().flattenToShortString();
		assertTrue(nextIntent.contains(".SpinnerActivity"));
	}
}
