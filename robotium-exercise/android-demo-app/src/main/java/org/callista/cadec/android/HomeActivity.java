package org.callista.cadec.android;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;

public class HomeActivity extends Activity {

	private static final String TAG = HomeActivity.class.getSimpleName();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		Log.i(TAG, "Starting home activity");
		setContentView(R.layout.home_layout);
		Log.i(TAG, "Finishing home activity");
	}
}
