package org.callista.cadec.android;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

/**
 * 
 * Handle the login for the android-demo-app application.
 * 
 */
public class LoginActivity extends Activity {

	private static final String TAG = LoginActivity.class.getSimpleName();

	private EditText ffUsername;
	private EditText ffPassword;
	private Button btnLogin;
	private Button btnCancel;
	private TextView lblResult;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.main);

		ffUsername = (EditText) findViewById(R.id.username);
		ffPassword = (EditText) findViewById(R.id.password);
		btnLogin = (Button) findViewById(R.id.login_button);
		btnCancel = (Button) findViewById(R.id.cancel_button);
		lblResult = (TextView) findViewById(R.id.result);

		btnLogin.setOnClickListener(mLoginListener);
		btnCancel.setOnClickListener(mCancelListener);
	}

	OnClickListener mCancelListener = new OnClickListener() {

		public void onClick(View v) {
			Log.d(TAG, "Cancel login");
			finish();
		}
	};

	OnClickListener mLoginListener = new OnClickListener() {

		public void onClick(View v) {

			final String username = ffUsername.getText().toString();
			final String password = ffPassword.getText().toString();

			Log.d(TAG, "Trying to login user " + username);

			if (userExist(username, password)) {
				gotoStartActivity(username);
			} else {
				userNotAuthorized(username);
			}
		}

		private void userNotAuthorized(final String username) {
			Log.d(TAG, "User " + username + " not authenticated");
			lblResult.setText(R.string.not_authenticated);
		}

		private void gotoStartActivity(final String username) {
			Log.d(TAG, "User " + username + " succesfully logged in, redirect to Home activity");
			Intent intent = new Intent(LoginActivity.this, SpinnerActivity.class);
			startActivity(intent);
			finish();
		}

		private boolean userExist(final String username, final String password) {
			return username.equals("me") && password.equals("me");
		}
	};
}
