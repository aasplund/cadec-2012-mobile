package org.callista.cadec.android.robotiumtest;

import junit.framework.TestCase;

public class LoginActivityRobotiumTest extends TestCase {

	/*
	 * Exercise:
	 * 
	 * Setup/TearDown Robotium.Solo
	 * 
	 * Implement the tests below
	 */

	public void testSuccessfulLoginTriggerMainActivity() {

	}

	public void testWrongUsernameTriggersErrorTextAtLogin() {

	}

	public void testWrongPasswordTriggersErrorTextAtLogin() {

	}

	public void dummy() {
		assertTrue(true);
	}
}
