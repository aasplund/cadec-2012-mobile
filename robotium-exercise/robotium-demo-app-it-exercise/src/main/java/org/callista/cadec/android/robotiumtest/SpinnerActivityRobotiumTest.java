package org.callista.cadec.android.robotiumtest;

import junit.framework.TestCase;

public class SpinnerActivityRobotiumTest extends TestCase {

	/*
	 * Exercise:
	 * 
	 * Setup/TearDown Robotium.Solo
	 * 
	 * Implement the tests below
	 */

	public void testScrollUpAndSelectNewItemFromSpinner() {

	}

	public void testScrollDownAndSelectNewItemFromSpinner() {

	}

	public void testSelectedItemIsStillSelectedAfterOrientationUpdated() {

	}

	public void dummy() {
		assertTrue(true);
	}
}
