package org.callista.cadec.android.robotiumtest;

import org.callista.cadec.android.LoginActivity;
import org.callista.cadec.android.SpinnerActivity;

import android.test.ActivityInstrumentationTestCase2;

import com.jayway.android.robotium.solo.Solo;

public class LoginActivityRobotiumTest extends ActivityInstrumentationTestCase2<LoginActivity> {

	private Solo solo;

	public LoginActivityRobotiumTest() throws ClassNotFoundException {
		super(LoginActivity.class);
	}

	@Override
	protected void setUp() throws Exception {
		solo = new Solo(getInstrumentation(), getActivity());
	}

	public void testSuccessfulLoginTriggerMainActivity() {
		clearUsernameAndPassword();
		enterUsernameAndPassword("me", "me");
		clickLoginButton();

		assertCurrentActivityIs(SpinnerActivity.class);
	}

	public void testWrongUsernameTriggersErrorTextAtLogin() {
		clearUsernameAndPassword();
		enterUsernameAndPassword("notMe", "notMyPassword");
		clickLoginButton();

		assertPageContainsText("Felaktigt användarnamn/lösenord");
		assertCurrentActivityIs(LoginActivity.class);
	}

	public void testWrongPasswordTriggersErrorTextAtLogin() {
		clearUsernameAndPassword();
		enterUsernameAndPassword("me", "notMyPassword");
		clickLoginButton();

		assertPageContainsText("Felaktigt användarnamn/lösenord");
		assertCurrentActivityIs(LoginActivity.class);
	}

	private void clearUsernameAndPassword() {
		solo.clearEditText(0);
		solo.clearEditText(1);
	}

	private void enterUsernameAndPassword(String username, String password) {
		solo.enterText(0, username);
		solo.enterText(1, password);
	}

	private void clickLoginButton() {
		solo.clickOnButton("Logga in");
	}

	private void assertPageContainsText(String text) {
		assertTrue(solo.searchText(text));
	}

	private void assertCurrentActivityIs(Class activity) {
		solo.assertCurrentActivity("Current activity should be " + activity.getName(), activity);
	}

	@Override
	public void tearDown() throws Exception {
		solo.finishOpenedActivities();
	}
}
