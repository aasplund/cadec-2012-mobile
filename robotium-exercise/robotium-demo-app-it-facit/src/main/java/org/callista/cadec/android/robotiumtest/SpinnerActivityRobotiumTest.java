package org.callista.cadec.android.robotiumtest;

import org.callista.cadec.android.SpinnerActivity;

import android.test.ActivityInstrumentationTestCase2;

import com.jayway.android.robotium.solo.Solo;

public class SpinnerActivityRobotiumTest extends ActivityInstrumentationTestCase2<SpinnerActivity> {

	private Solo solo;

	public SpinnerActivityRobotiumTest() throws ClassNotFoundException {
		super(SpinnerActivity.class);
	}

	@Override
	protected void setUp() throws Exception {
		solo = new Solo(getInstrumentation(), getActivity());
	}

	public void testScrollUpAndSelectNewItemFromSpinner() {
		solo.sendKey(Solo.ENTER);
		solo.scrollUp();
		solo.clickOnText("Mercury");
		assertTrue(solo.isSpinnerTextSelected("Mercury"));
	}

	public void testScrollDownAndSelectNewItemFromSpinner() {
		solo.sendKey(Solo.ENTER);
		solo.scrollDown();
		solo.clickOnText("Pluto");
		assertTrue(solo.isSpinnerTextSelected("Pluto"));
	}

	public void testSelectedItemIsStillSelectedAfterOrientationUpdated() {
		solo.sendKey(Solo.ENTER);
		solo.setActivityOrientation(Solo.LANDSCAPE);
		solo.sendKey(Solo.ENTER);

		solo.scrollUp();
		solo.clickOnText("Venus");
		assertTrue(solo.isSpinnerTextSelected("Venus"));
	}

	@Override
	public void tearDown() throws Exception {
		solo.finishOpenedActivities();
	}
}
